/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_core_deleter_h
#define _tairon_net_core_deleter_h

namespace Tairon
{

namespace Net
{

/** \brief Basic class for deleters that are used to delete objects.
 */
class BasicDeleter
{
	public:
		/** Standard constructor; does nothing.
		 */
		BasicDeleter() {};

		/** Virtual destructor.
		 */
		virtual ~BasicDeleter() {};
};

/** \brief Deleters are used to delete object.
 */
template<class TObject> class Deleter : public BasicDeleter
{
	public:
		/** Creates a Deleter object.
		 */
		Deleter(TObject *obj) : object(obj) {};

		/** Destroys the deleter and its object.
		 */
		virtual ~Deleter() {
			delete object;
		};

	private:
		/** Object that should be deleted.
		 */
		TObject *object;
};

/** Just convenient wrapper for creating Deleter objects.
 */
template<class TObject> inline Deleter<TObject> *createDeleter(TObject *object)
{
	return new Deleter<TObject>(object);
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
