/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_core_socket_h
#define _tairon_net_core_socket_h

#include <sys/socket.h>

#include <tairon/core/exceptions.h>
#include <tairon/core/signals.h>

using Tairon::Core::String;

namespace Tairon
{

namespace Net
{

class SocketNotifier;

/** \brief This class provides non-blocking network connection.
 *
 * Socket's family can be Unix for local communication through unix sockets or
 * IPv4 for communication using protocol with the same name.
 *
 * Socket type can be either a stream (for reliable connection-based byte
 * streams) or a datagram (for connectionless, unreliable messages). When new
 * data is incoming readyRead signal is emitted and the data can be read with
 * readAll() method which reads all the data from socket. In some cases better
 * performance can be achieved with readTo() method which reads not more than
 * given amount of data into specified location in memory (this can be a mmaped
 * file, so no data copying occurs at all).
 *
 * If an error occurs either an exception is thrown or signal is emitted. It is
 * necessary to close the socket in both cases. The socket is deleted by
 * Poll::poll() after it is closed.
 */
class Socket
{
	public:
		/** Family of the socket.
		 */
		enum Family {
			/** Unix socket (only local connections).
			 */
			Unix = PF_UNIX,

			/** IPv4 socket.
			 */
			IPv4 = PF_INET
		};

		/** Type of the socket.
		 */
		enum Type {
			/** Stream data (TCP connection).
			 */
			Stream = SOCK_STREAM,

			/** Datagrams (UDP packets).
			 */
			Datagram = SOCK_DGRAM
		};

		static const size_t bufLength;

		/** Creates new socket of given type.
		 */
		Socket(Family family, Type type);

		/** Creates new socket object associated with given descriptor.
		 */
		Socket(int sd);

		/** Closes socket and frees all resources.
		 */
		~Socket();

		/** Closes the socket and destroys the object.
		 */
		void close();

		/** Connects this socket to a unix socket.
		 *
		 * \param path Path to the unix socket.
		 */
		void connect(const String &path);

		/** Connects this socket (with IPv4 family) to a host.
		 *
		 * \param address Host to which this socket should be connected.
		 * \param port Target port of the connection in host byte order.
		 */
		void connect(const String &address, uint16_t port);

		/** Returns descriptor of this socket.
		 */
		int getDescriptor() {
			return fd;
		};

		/** Reads data from the socket. This method never blocks. This
		 * method throws a SocketException when the socket has been closed.
		 *
		 * \param max number of bytes to read. If this number is greater than
		 * the size of the internal buffer, then data that fits into the
		 * internal buffer are returned.
		 */
		String read(size_t max = bufLength);

		/** Reads data from the socket and stores them in the buffer. This
		 * method throws a SocketException when the socket has been closed.
		 *
		 * \param buf Target buffer for the data.
		 * \param length Length of the buffer.
		 */
		size_t readTo(void *buf, size_t length);

		/** Tells this socket that all setup has been done.
		 */
		void ready();

		/** Writes data to the socket and returns the number of characters
		 * written to the socket.
		 *
		 * \param data Data to write.
		 * \param block If it is set to true then the operation will block when
		 * it cannot be processed completely.
		 */
		size_t write(const String &data, bool block = true);

		/** Writes data to the socket and returns the number of characters
		 * written to the socket.
		 *
		 * \param data Data to write.
		 * \param length Length of data.
		 * \param block If it is set to true then the operation will block when
		 * it cannot be processed completely.
		 */
		size_t write(const char *data, size_t length, bool block = true);

	public:
		/** This signal is emitted when the socket is connected to the host.
		 * The receiver gets as the first parameter pointer to this socket.
		 */
		Tairon::Core::DSignal1<Socket *> connectedSignal;

		/** This signal is emitted when an error occurs. The receiver gets as
		 * the first parameter pointer to this socket and the second one is
		 * error code. The receiver must close the socket after receiving this
		 * signal.
		 */
		Tairon::Core::DSignal2<Socket *, int> errorSignal;

		/** This signal is emitted when there is incoming data. The receiver
		 * gets as the first parameter pointer to this socket.
		 */
		Tairon::Core::DSignal1<Socket *> readyReadSignal;

		/** This signal is emitted when the socket is ready to write data. The
		 * receiver gets as the first parameter pointer to this socket.
		 */
		Tairon::Core::DSignal1<Socket *> readyWriteSignal;

	private:
		/** Initializes private data.
		 */
		inline void init();

		/** Calls connected signal with this class as the first parameter.
		 */
		void connected();

		/** Gets an error number end emits errorSignal.
		 */
		void error();

		/** Invokes ready read signal with this class as the first parameter.
		 */
		void readyRead();

		/** Cals readyWriteSignal.
		 */
		void readyWrite();

	private:
		/** Functor for calling connected method.
		 */
		Tairon::Core::Functor0<void> *connectedFunctor;

		/** Socket descriptor.
		 */
		int fd;

		/** Buffer for input data.
		 */
		char *inBuffer;

		/** Notifier of this socket.
		 */
		SocketNotifier *notifier;

		/** Functor for calling readyWrite method.
		 */
		Tairon::Core::Functor0<void> *readyWriteFunctor;

	friend class Tairon::Core::MethodFunctor0<void, Socket>;
};

/** \brief Exception for socket errors.
 */
class SocketException : public Tairon::Core::Exception
{
	public:
		/** Constructor that takes as parameters description of the exception
		 * and its error number.
		 */
		SocketException(const String &desc, int err);

		/** Standard destructor.
		 */
		virtual ~SocketException() {};

		/** Returns error number of the exception.
		 */
		int getErrorNumber() const {
			return errorNumber;
		};

	private:
		/** Error number of this exception.
		 */
		int errorNumber;
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
