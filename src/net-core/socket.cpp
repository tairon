/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include <sys/un.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>

#include <tairon/core/log.h>

#include "socket.h"

#include "deleter.h"
#include "poll.h"
#include "socketnotifier.h"

namespace Tairon
{

namespace Net
{

const size_t Socket::bufLength = 65536;

/* {{{ Socket::Socket(Family, Type) */
Socket::Socket(Family family, Type type)
{
	fd = socket(family, type, 0);
	if (fd == -1)
		throw SocketException("Cannot create socket", errno);

	init();
}
/* }}} */

/* {{{ Socket::Socket(int sd) */
Socket::Socket(int sd) : fd(sd)
{
	init();

	notifier->setReadyReadFunctor(Tairon::Core::methodFunctor(this, &Socket::readyRead));
	notifier->setReadyWriteFunctor(Tairon::Core::methodFunctor(this, &Socket::readyWrite));
}
/* }}} */

/* {{{ Socket::~Socket() */
Socket::~Socket()
{
	// deleting notifier also deletes all created functors
	delete notifier;
	delete [] inBuffer;
}
/* }}} */

/* {{{ Socket::close() */
void Socket::close()
{
	connectedSignal.disable();
	errorSignal.disable();
	readyReadSignal.disable();
	readyWriteSignal.disable();

	// this also removes socket from polling
	::close(fd);

	Poll::self()->addDeleter(createDeleter(this));
}
/* }}} */

/* {{{ Socket::connected() */
void Socket::connected()
{
	DEBUG((const char *) String("Socket::connected " + String::number(fd)));

	fcntl(fd, F_SETFL, fcntl(fd, F_GETFL, 0) & (!O_NONBLOCK));

	Poll::self()->unregisterSocket(fd);
	delete notifier->getReadyWriteFunctor();
	notifier->setReadyWriteFunctor(Tairon::Core::methodFunctor(this, &Socket::readyWrite));

	int err;
	socklen_t len = sizeof(err);
	getsockopt(fd, SOL_SOCKET, SO_ERROR, &err, &len);
	if (err == 0) {
		notifier->setReadyReadFunctor(Tairon::Core::methodFunctor(this, &Socket::readyRead));
		// don't register socket for reading, wait for the ready method
		connectedSignal.emit(this);
	} else
		errorSignal.emit(this, err);
}
/* }}} */

/* {{{ Socket::connect(const String &) */
void Socket::connect(const String &path)
{
	if (path.length() >= 108) // length of struct socaddr_un.sun_path field
		throw SocketException("Path for unix socket is too long", 0);

	if (fcntl(fd, F_SETFL, fcntl(fd, F_GETFL, 0) | O_NONBLOCK) == -1)
		throw SocketException("Cannot set nonblocking socket", errno);

	sockaddr_un addr;
	addr.sun_family = AF_UNIX;
	memcpy(addr.sun_path, path.data(), path.length());
	addr.sun_path[path.length()] = 0;

	int ret = ::connect(fd, (sockaddr *) &addr, sizeof(addr));
	if ((ret == -1) && (errno != EINPROGRESS))
		throw SocketException("Error while connecting", errno);

	notifier->setReadyWriteFunctor(Tairon::Core::methodFunctor(this, &Socket::connected));

	Poll::self()->registerSocketToWrite(fd, notifier);
}
/* }}} */

/* {{{ Socket::connect(const String &, uint16_t) */
void Socket::connect(const String &address, uint16_t port)
{
	if (fcntl(fd, F_SETFL, fcntl(fd, F_GETFL, 0) | O_NONBLOCK) == -1)
		throw SocketException("Cannot set nonblocking socket", errno);

	struct hostent *host = gethostbyname(address);
	if (!host)
		throw SocketException("Cannot resolve host name", h_errno);

	if (host->h_addrtype != AF_INET)
		throw SocketException("Invalid address type", host->h_addrtype);

	if (!(*host->h_addr_list))
		throw SocketException("Zero length list of addresses", 0);

	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	memcpy(&(addr.sin_addr.s_addr), *(host->h_addr_list), sizeof(addr.sin_addr.s_addr));

	int ret = ::connect(fd, (sockaddr *) &addr, sizeof(addr));
	if ((ret == -1) && (errno != EINPROGRESS))
		throw SocketException("Error while connecting", errno);

	notifier->setReadyWriteFunctor(Tairon::Core::methodFunctor(this, &Socket::connected));

	Poll::self()->registerSocketToWrite(fd, notifier);
}
/* }}} */

/* {{{ Socket::error() */
void Socket::error()
{
	int err;
	socklen_t len = sizeof(err);
	getsockopt(fd, SOL_SOCKET, SO_ERROR, &err, &len);
	Poll::self()->unregisterSocket(fd);
	errorSignal.emit(this, err);
}
/* }}} */

/* {{{ Socket::init() */
void Socket::init()
{
	inBuffer = new char[bufLength];
	notifier = new SocketNotifier(0, 0, Tairon::Core::methodFunctor(this, &Socket::error));
}
/* }}} */

/* {{{ Socket::read(size_t) */
String Socket::read(size_t max)
{
	ssize_t len = recv(fd, inBuffer, bufLength < max ? bufLength : max, MSG_DONTWAIT);
	if (len == 0) // connection has been closed
		throw SocketException("Connection has been closed", 0);
	else if (len < 0)
		if ((errno != EAGAIN) && (errno != EINTR)) {
			throw SocketException("Error while reading from socket", errno);
		} else
			len = 0;
	try {
		Poll::self()->addSocketToRead(fd, notifier);
	} catch (const PollException &) { // do nothing
	}
	return String(inBuffer, len);
}
/* }}} */

/* {{{ Socket::readyRead() */
void Socket::readyRead()
{
	DEBUG((const char *) String("Socket::readyRead " + String::number((int) this)));

	// don't make signal storm - if processing of the signals takes much time
	// in another thread, then there can be new data and many new signals would
	// be emitted
	try {
		Poll::self()->delSocketToRead(fd, notifier);
	} catch (const PollException &) {
	}
	readyReadSignal.emit(this);
}
/* }}} */

/* {{{ Socket::readTo(void *, size_t) */
size_t Socket::readTo(void *buf, size_t length)
{
	ssize_t len = recv(fd, buf, length, MSG_DONTWAIT);
	if (len == 0) // connection has been closed
		throw SocketException("Connection has been closed", 0);
	else if (len < 0)
		if ((errno != EAGAIN) && (errno != EINTR))
			throw SocketException("Error while reading from socket", errno);
		else
			len = 0;
	try {
		Poll::self()->addSocketToRead(fd, notifier);
	} catch (const PollException &) { // do nothing
	}
	return len;
}
/* }}} */

/* {{{ Socket::ready() */
void Socket::ready()
{
	Poll::self()->registerSocketToRead(fd, notifier);
}
/* }}} */

/* {{{ Socket::readyWrite() */
void Socket::readyWrite()
{
	Poll::self()->delSocketToWrite(fd, notifier);
	readyWriteSignal.emit(this);
}
/* }}} */

/* {{{ Socket::write(const String &, bool) */
size_t Socket::write(const String &data, bool block)
{
	return write(data.data(), data.length(), block);
}
/* }}} */

/* {{{ Socket::write(const char *, size_t, bool) */
size_t Socket::write(const char *data, size_t length, bool block)
{
	ssize_t sent = send(fd, data, length, block ? 0 : MSG_DONTWAIT);
	if (sent == -1) {
		if ((errno != EAGAIN) && (errno != EINTR))
			throw SocketException("Error while writing to socket", errno);
		try {
			Poll::self()->addSocketToWrite(fd, notifier);
		} catch (const PollException &) { // do nothing
		}
		return 0;
	}
	return sent;
}
/* }}} */


/* {{{ SocketException::SocketException(const String &, int) */
SocketException::SocketException(const String &desc, int err) : Tairon::Core::Exception(desc), errorNumber(err)
{
	DEBUG((const char *) String(desc + " (" + String::number(errorNumber) + ")"));
}
/* }}} */

};

};

// vim: ai sw=4 ts=4 noet fdm=marker
