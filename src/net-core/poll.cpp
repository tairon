/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include <errno.h>

#include <tairon/core/log.h>
#include <tairon/core/mutex.h>

#include "poll.h"

#include "deleter.h"
#include "socketnotifier.h"

namespace Tairon
{

namespace Net
{

Poll *Poll::pollInstance = 0;

/* {{{ Poll::Poll(size_t) */
Poll::Poll(size_t maxSockets) : eventsSize(maxSockets)
{
	if (Poll::pollInstance)
		throw Tairon::Core::SingletonException("Cannot make another instance of Tairon::Net::Poll");

	epfd = epoll_create(maxSockets);
	if (epfd == -1)
		throw PollException("Cannot create epoll descriptor", errno);

	deletersMutex = new Tairon::Core::Mutex();

	events = new epoll_event[maxSockets];
	Poll::pollInstance = this;
}
/* }}} */

/* {{{ Poll::~Poll() */
Poll::~Poll()
{
	deleteObjects();
	delete deletersMutex;

	close(epfd);
	Poll::pollInstance = 0;
	delete [] events;
}
/* }}} */

/* {{{ Poll::addSocketToRead(int, SocketNotifier *) */
void Poll::addSocketToRead(int fd, SocketNotifier *notifier)
{
	DEBUG((const char *) String("Poll::addSocketToRead " + String::number(fd)));

	notifier->events |= EPOLLIN;
	changeSocket(fd, notifier);
}
/* }}} */

/* {{{ Poll::addSocketToWrite(int, SocketNotifier *) */
void Poll::addSocketToWrite(int fd, SocketNotifier *notifier)
{
	DEBUG((const char *) String("Poll::addSocketToWrite " + String::number(fd)));

	notifier->events |= EPOLLOUT;
	changeSocket(fd, notifier);
}
/* }}} */

/* {{{ Poll::changeSocket(int, SocketNotifier *notifier) */
void Poll::changeSocket(int fd, SocketNotifier *notifier)
{
	epoll_event event;
	event.events = notifier->events;
	event.data.ptr = notifier;
	if (epoll_ctl(epfd, EPOLL_CTL_MOD, fd, &event) == -1)
		throw PollException("Cannot change socket in epoll", errno);
}
/* }}} */

/* {{{ Poll::deleteObjects() */
void Poll::deleteObjects()
{
	deletersMutex->lock();
	for (std::list<BasicDeleter *>::iterator it = deleters.begin(); it != deleters.end(); ++it)
		delete *it;
	deleters.clear();
	deletersMutex->unlock();
}
/* }}} */

/* {{{ Poll::delSocketToRead(int, SocketNotifier *) */
void Poll::delSocketToRead(int fd, SocketNotifier *notifier)
{
	notifier->events &= ~EPOLLIN;
	changeSocket(fd, notifier);
}
/* }}} */

/* {{{ Poll::delSocketToWrite(int, SocketNotifier *) */
void Poll::delSocketToWrite(int fd, SocketNotifier *notifier)
{
	notifier->events &= ~EPOLLOUT;
	changeSocket(fd, notifier);
}
/* }}} */

/* {{{ Poll::poll(int) */
void Poll::poll(int timeout)
{
	int count = epoll_wait(epfd, events, eventsSize, timeout);
	if (count == -1)
		if (errno != EINTR)
			throw PollException("Error while waiting for epoll", errno);

	SocketNotifier *notifier;
	for (int i = 0; i < count; ++i) {
		notifier = (SocketNotifier *) events[i].data.ptr;
		if (events[i].events & (EPOLLERR | EPOLLHUP)) {
			if (notifier->errorFunctor)
				(*notifier->errorFunctor)();
			continue;
		}
		if (events[i].events & EPOLLIN)
			(*notifier->readyReadFunctor)();
		if (events[i].events & EPOLLOUT)
			(*notifier->readyWriteFunctor)();
	}

	deleteObjects();
}
/* }}} */

/* {{{ Poll::registerSocketToRead(int, SocketNotifier *) */
void Poll::registerSocketToRead(int fd, SocketNotifier *notifier)
{
	epoll_event event;
	event.events = EPOLLIN;
	event.data.ptr = notifier;
	if (epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &event) == -1)
		throw PollException("Cannot add socket to epoll", errno);
}
/* }}} */

/* {{{ Poll::registerSocketToWrite(int, SocketNotifier *) */
void Poll::registerSocketToWrite(int fd, SocketNotifier *notifier)
{
	epoll_event event;
	event.events = EPOLLOUT;
	event.data.ptr = notifier;
	if (epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &event) == -1)
		throw PollException("Cannot add socket to epoll", errno);
}
/* }}} */

/* {{{ Poll::unregisterSocket(int) */
void Poll::unregisterSocket(int fd)
{
	epoll_ctl(epfd, EPOLL_CTL_DEL, fd, 0);
}
/* }}} */


/* {{{ PollException::PollException(const String &, int) */
PollException::PollException(const String &desc, int err) : Tairon::Core::Exception(desc), errorNumber(err)
{
	DEBUG((const char *) String(desc + " (" + String::number(err) + ")"));
}
/* }}} */

}; // namespace Net

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
