/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_core_socketnotifier_h
#define _tairon_net_core_socketnotifier_h

#include <tairon/core/signals.h>

namespace Tairon
{

namespace Net
{

/** \brief Object used by Tairon::Net::Poll to signal changes on a descriptor.
 */
class SocketNotifier
{
	public:
		/** Creates a SocketNotifier object.
		 *
		 * \param rrFunctor Functor for ready read signal.
		 * \param rwFunctor Functor for ready write signal.
		 * \param errFunctor Functor for error signal.
		 */
		SocketNotifier(Tairon::Core::Functor0<void> *rrFunctor, Tairon::Core::Functor0<void> *rwFunctor, Tairon::Core::Functor0<void> *errFunctor);

		/** Destroys the object and deletes all associated functors.
		 */
		~SocketNotifier();

		/** Returns functor for error signal.
		 */
		Tairon::Core::Functor0<void> *getErrorFunctor() {
			return errorFunctor;
		};

		/** Returns functor for ready read signal.
		 */
		Tairon::Core::Functor0<void> *getReadyReadFunctor() {
			return readyReadFunctor;
		};

		/** Returns functor for ready write signal.
		 */
		Tairon::Core::Functor0<void> *getReadyWriteFunctor() {
			return readyWriteFunctor;
		};

		/** Sets functor for error signal. Previous functor (if it has been
		 * set) is not deleted.
		 */
		void setErrorFunctor(Tairon::Core::Functor0<void> *functor) {
			errorFunctor = functor;
		};

		/** Sets functor for ready read signal. Previous functor (if it has
		 * been set) is not deleted.
		 */
		void setReadyReadFunctor(Tairon::Core::Functor0<void> *functor) {
			readyReadFunctor = functor;
		};

		/** Sets functor for ready write signal. Previous functor (if it has
		 * been set) is not deleted.
		 */
		void setReadyWriteFunctor(Tairon::Core::Functor0<void> *functor) {
			readyWriteFunctor = functor;
		};

	private:
		/** Functor for dispatching error signal.
		 */
		Tairon::Core::Functor0<void> *errorFunctor;

		/** Events this notifier is wating for.
		 */
		uint32_t events;

		/** Functor for dispatching ready read signal.
		 */
		Tairon::Core::Functor0<void> *readyReadFunctor;

		/** Functor for dispatching ready write signal.
		 */
		Tairon::Core::Functor0<void> *readyWriteFunctor;

	friend class Poll;
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
