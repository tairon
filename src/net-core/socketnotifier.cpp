/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include "socketnotifier.h"

namespace Tairon
{

namespace Net
{

/* {{{ SocketNotifier::SocketNotifier(3x Tairon::Core::Functor0<void> *) */
SocketNotifier::SocketNotifier(Tairon::Core::Functor0<void> *rrFunctor, Tairon::Core::Functor0<void> *rwFunctor, Tairon::Core::Functor0<void> *errFunctor) : errorFunctor(errFunctor), events(0), readyReadFunctor(rrFunctor), readyWriteFunctor(rwFunctor)
{
}
/* }}} */

/* {{{ SocketNotifier::~SocketNotifier() */
SocketNotifier::~SocketNotifier()
{
	delete errorFunctor;
	delete readyReadFunctor;
	delete readyWriteFunctor;
}
/* }}} */

}; // namespace Net

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
