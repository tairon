/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_core_loop_h
#define _tairon_net_core_loop_h

#include <map>

namespace Tairon
{

namespace Core
{

class Mutex;

}; // namespace Core;

namespace Net
{

class Poll;
class Timer;

/** \brief This class provides loop for networking and timers.
 *
 * Loop creates Poll object, so it is not needed to create another one.
 */
class Loop
{
	public:
		/** Initializes the loop.
		 */
		Loop();

		/** Destroys the loop.
		 */
		~Loop();

		/** Terminates the loop as soon as possible.
		 */
		void exit() {
			e = true;
		};

		/** Executes the loop. The loop only calls Poll::poll() and takes care
		 * of timers, it doesn't try to call pending functors in this thread
		 * because it shouldn't be necessary.
		 */
		void run();

		/** Returns the instance of this class.
		 */
		static Loop *self() {
			return loop;
		};

	private:
		/** Registers new timer.
		 */
		void registerTimer(Timer *timer);

		/** Unregisters a timer.
		 */
		void unregisterTimer(Timer *timer);

	private:
		/** When the loop should exit this is set to true.
		 */
		bool e;

		/** Pointer to the instance of this class.
		 */
		static Loop *loop;

		/** Polling object.
		 */
		Poll *poll;

		/** Mapping timeout->timer.
		 */
		std::multimap<uint64_t, Timer *> timers;

		/** Mutex for locking timers structure.
		 */
		Tairon::Core::Mutex *timersMutex;

	friend class Timer;
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
