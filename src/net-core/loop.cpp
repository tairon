/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include <tairon/core/exceptions.h>
#include <tairon/core/mutex.h>

#include "loop.h"

#include "poll.h"
#include "timer.h"

namespace Tairon
{

namespace Net
{

Loop *Loop::loop = 0;

/* {{{ Loop::Loop() */
Loop::Loop() : e(false)
{
	if (loop)
		throw Tairon::Core::SingletonException("Cannot create another instance of Tairon::Net::Loop");

	timersMutex = new Tairon::Core::Mutex();
	poll = new Tairon::Net::Poll(2);

	loop = this;
}
/* }}} */

/* {{{ Loop::~Loop() */
Loop::~Loop()
{
	delete poll;
	delete timersMutex;
	loop = 0;
}
/* }}} */

/* {{{ Loop::registerTimer(Timer *) */
void Loop::registerTimer(Timer *timer)
{
	timersMutex->lock();
	timers.insert(std::pair<uint64_t, Timer *>(timer->nextTimeout, timer));
	timersMutex->unlock();
}
/* }}} */

/* {{{ Loop::run() */
void Loop::run()
{
	while (!e) {
		int timeout;

		timersMutex->lock();
		if (timers.size())
			timeout = timers.begin()->first - Timer::currentTime();
		else
			timeout = 100;
		timersMutex->unlock();

		if (timeout < 0)
			timeout = 0;
		else if (timeout > 100) // 100 miliseconds
			timeout = 100;

		poll->poll(timeout);

		uint64_t t = Timer::currentTime();
		timersMutex->lock();
		std::multimap<uint64_t, Timer *>::iterator it = timers.begin();
		while (timers.size() && (it->first <= t)) {
			Timer *timer = it->second;
			timers.erase(it);

			timersMutex->unlock();
			timer->timeout();
			timersMutex->lock();

			it = timers.begin();
		}
		timersMutex->unlock();
	}
}
/* }}} */

/* {{{ Loop::unregisterTimer(Timer *timer) */
void Loop::unregisterTimer(Timer *timer)
{
	timersMutex->lock();
	for (std::multimap<uint64_t, Timer *>::iterator it = timers.lower_bound(timer->nextTimeout); it != timers.upper_bound(timer->nextTimeout); ++it)
			if (it->second == timer) {
				timers.erase(it);
				break;
			}
	timersMutex->unlock();
}
/* }}} */

}; // namespace Net

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
