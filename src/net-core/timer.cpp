/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include <sys/time.h>

#include "timer.h"

#include "deleter.h"
#include "loop.h"
#include "poll.h"

namespace Tairon
{

namespace Net
{

/* {{{ Timer::Timer() */
Timer::Timer() : nextTimeout(0)
{
}
/* }}} */

/* {{{ Timer::~Timer() */
Timer::~Timer()
{
}
/* }}} */

/* {{{ Timer::currentTime() */
uint64_t Timer::currentTime()
{
	struct timeval tv;
	gettimeofday(&tv, 0);

	return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}
/* }}} */

/* {{{ Timer::destroy() */
void Timer::destroy()
{
	stop();
	Poll::self()->addDeleter(createDeleter(this));
}
/* }}} */

/* {{{ Timer::start(unsigned int, bool sshot) */
void Timer::start(unsigned int msec, bool sshot)
{
	if (nextTimeout != 0)
		stop();

	interval = msec;
	nextTimeout = currentTime() + msec;
	singleShot = sshot;

	Loop::self()->registerTimer(this);
}
/* }}} */

/* {{{ Timer::stop() */
void Timer::stop()
{
	timeoutSignal.disable();
	if (!nextTimeout)
		return;

	Loop::self()->unregisterTimer(this);
	nextTimeout = 0;
}
/* }}} */

/* {{{ Timer::timeout() */
void Timer::timeout()
{
	if (!nextTimeout)
		return;

	if (singleShot)
		nextTimeout = 0;
	else {
		nextTimeout += interval;
		Loop::self()->registerTimer(this);
	}

	timeoutSignal.emit();
}
/* }}} */

}; // namespace Net

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
