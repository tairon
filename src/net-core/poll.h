/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_core_poll_h
#define _tairon_net_core_poll_h

#include <sys/epoll.h>

#include <tairon/core/exceptions.h>
#include <tairon/core/signals.h>

using Tairon::Core::String;

namespace Tairon
{

namespace Core
{

class Mutex;

}; // namespace Core

namespace Net
{

class BasicDeleter;
class SocketNotifier;

/** \brief I/O event notification facility.
 *
 * This class provides interface to epoll(7) functionality.
 */
class Poll
{
	public:
		/** Creates Poll object.
		 *
		 * \param maxSockets Size of the internal table of descriptors.
		 */
		Poll(size_t maxSockets);

		/** Destroys the object.
		 */
		~Poll();

		/** Adds deleter that will delete object. This is used to avoid race
		 * condition at time when poll calls functors of objects that would be
		 * otherwise deleted.
		 */
		void addDeleter(BasicDeleter *deleter) {
			deletersMutex->lock();
			deleters.push_back(deleter);
			deletersMutex->unlock();
		};

		/** Adds socket to be available to read operation.
		 *
		 * \param fd Descriptor of the socket.
		 * \param notifier Notifier that will be called when the event occurs.
		 */
		void addSocketToRead(int fd, SocketNotifier *notifier);

		/** Adds socket to be available to write operation.
		 *
		 * \param fd Descriptor of the socket.
		 * \param notifier Notifier that will be called when the event occurs.
		 */
		void addSocketToWrite(int fd, SocketNotifier *notifier);

		/** Deletes socket from read event notifications.
		 *
		 * \param fd Descriptor of the socket to delete.
		 * \param notifier Notifier of the socket.
		 */
		void delSocketToRead(int fd, SocketNotifier *notifier);

		/** Deletes socket from write event notifications.
		 *
		 * \param fd Descriptor of the socket to delete.
		 * \param notifier Notifier of the socket.
		 */
		void delSocketToWrite(int fd, SocketNotifier *notifier);

		/** Waits for events to occur.
		 *
		 * \param timeout Specifies how many miliseconds should this method
		 * wait for an event. Specifying a timeout of -1 makes this method wait
		 * indefinitely, while specifying a timeout equal to zero makes this
		 * method return immediately even if no events are available.
		 */
		void poll(int timeout);

		/** Returns pointer to the instance of this class.
		 */
		static Poll *self() {
			return pollInstance;
		};

		/** Registers socket to the polling system for read event. The socket
		 * is removed from the polling system by closing it or by calling
		 * unregisterSocket().
		 */
		void registerSocketToRead(int fd, SocketNotifier *notifier);

		/** Registers socket to the polling system for write event. The socket
		 * is removed from the polling system by closing it or by calling
		 * unregisterSocket().
		 */
		void registerSocketToWrite(int fd, SocketNotifier *notifier);

		/** Unregisters a socket from the polling system.
		 */
		void unregisterSocket(int fd);

	private:
		/** Changes a socket in the epoll.
		 *
		 * \param fd Descriptor of the socket.
		 * \param notifier Notifier of the socket.
		 */
		void changeSocket(int fd, SocketNotifier *notifier);

		/** Delets waiting deleters.
		 */
		inline void deleteObjects();

	private:
		/** List of pending deleters.
		 */
		std::list<BasicDeleter *> deleters;

		/** Mutex for locking deleters.
		 */
		Tairon::Core::Mutex *deletersMutex;

		/** Epoll descriptor.
		 */
		int epfd;

		/** Buffer for the catched events.
		 */
		struct epoll_event *events;

		/** Size of the events array.
		 */
		size_t eventsSize;

		/** Holds pointer to the instance of this class.
		 */
		static Poll *pollInstance;
};

/** \brief Exception for poll errors.
 */
class PollException : public Tairon::Core::Exception
{
	public:
		/** Constructor that takes as parameters description of the exception
		 * and its error number.
		 */
		PollException(const String &desc, int err);

		/** Standard destructor.
		 */
		virtual ~PollException() {};

		/** Returns error number of the exception.
		 */
		int getErrorNumber() const {
			return errorNumber;
		};

	private:
		/** Error number of this exception.
		 */
		int errorNumber;
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
