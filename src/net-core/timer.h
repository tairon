/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_core_timer_h
#define _tairon_net_core_timer_h

#include <tairon/core/signals.h>

namespace Tairon
{

namespace Net
{

/** \brief This class provides timer signals and single-shot timers.
 *
 * Usage of Timer is very simple: create a Timer, connect timeoutSignal to
 * appropriate functor and call start() method to start the timer. When the
 * time is up it will emit the timeoutSignal.
 *
 * Deleting Timer must be done by calling destroy() method, not by using delete
 * operator; otherwise a condition race may happen.
 */
class Timer
{
	public:
		/** Constructs a new timer.
		 */
		Timer();

		/** Destroys the timer. This destructor mustn't be called directly, use
		 * destroy() for freeing memory.
		 */
		~Timer();

		/** Returns the time since the Epoch (00:00:00 UTC, January 1, 1970),
		 * measured in miliseconds.
		 */
		static uint64_t currentTime();

		/** Deletes this timer.
		 */
		void destroy();

		/** Starts the timer.
		 *
		 * \param msec Time to count in miliseconds.
		 * \param sshot Whether this timer is a single-shot one.
		 */
		void start(unsigned int msec, bool sshot = false);

		/** Stops the timer.
		 */
		void stop();

	public:
		/** This signal is emitted when the time is up.
		 */
		Tairon::Core::DSignal0 timeoutSignal;

	private:
		/** Called by the Poll when the time is up.
		 */
		void timeout();

	private:
		/** Timeout interval for this timer.
		 */
		unsigned int interval;

		/** When the next timeout should occur.
		 */
		uint64_t nextTimeout;

		/** Whether this timer is only single-shot.
		 */
		bool singleShot;

	friend class Loop;
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
