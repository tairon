/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_core_thread_h
#define _tairon_core_thread_h

#include <list>
#include <pthread.h>

#include "exceptions.h"
#include "mutex.h"
#include "semaphore.h"
#include "string.h"

namespace Tairon
{

namespace Core
{

class ThreadFunctorCaller;

/** \brief Class for creating new threads and dispatching signals.
 *
 * New thread is done with inheriting this class and implementing run() method
 * whish is entry point of new thread. The thread is started with start()
 * method.
 */
class Thread
{
	public:
		/** Creates new thread object with give name. This name can be used to
		 * identify the thread among others.
		 */
		Thread(const String &name);

		/** Destroys the thread object.
		 */
		virtual ~Thread();

		/** Returns reference to the current thread.
		 */
		static Thread *current();

		/** Returns name of this thread.
		 */
		const String &getName() {
			return name;
		}

		/** Make calling thread wait for termination of this thread.
		 */
		void *join();

		/** This method is called when new thread is created.
		 */
		virtual void *run() = 0;

		/** Stars new thread in which the run() method is called.
		 */
		void start();

		/** Appends to the list of callers another one.
		 */
		void addFunctorCaller(ThreadFunctorCaller *caller) {
			callersMutex->lock();
			callers.push_back(caller);
			callersMutex->unlock();
			callersSemaphore->post();
		};

	protected:
		/** Calls all functor callers waiting in queue.
		 */
		void callFunctors();

		/** Calls all functor callers waiting in queue. If the queue is empty
		 * then this method suspends calling thread until some functor caller
		 * arrives. This method waits no more than specified time.
		 */
		void waitAndCallFunctors(time_t seconds, unsigned long nanoseconds);

	private:
		/** Called by startFunction when the thread is started.
		 */
		void started();

		/** Called by startFunction when the thread finished.
		 */
		void finished();

	private:
		/** List of callers (objects that actually call methods) that should be
		 * executed in this thread.
		 */
		std::list<ThreadFunctorCaller *> callers;

		/** Mutex for locking list of callers.
		 */
		Mutex *callersMutex;

		/** Semaphore counting waiting callers.
		 */
		Semaphore *callersSemaphore;

		/** Name of this thread.
		 */
		String name;

		/** ID of this thread.
		 */
		pthread_t tid;

	friend class ThreadManager;
	friend void *startFunction(void *);
};

/** \brief Exception for thread errors.
 */
class ThreadException : public Exception
{
	public:
		/** Standard constructor.
		 */
		ThreadException(const String &desc) : Exception(desc) {};

		/** Default destructor.
		 */
		virtual ~ThreadException() {};
};

}; // namespace Core

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
