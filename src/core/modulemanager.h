/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_core_modulemanager_h
#define _tairon_core_modulemanager_h

#include <list>
#include <map>

#include "exceptions.h"
#include "signals.h"
#include "string.h"

namespace Tairon
{

namespace Core
{

class Module;

/** \brief Class that calculates dependencies of modules.
 *
 * It can be used only by the ModuleManager class.
 */
class ModuleDependencies
{
	private:
		/** Prepares calculation of the dependencies.
		 */
		ModuleDependencies(const String &filename);

		/** Destroys the object.
		 */
		~ModuleDependencies();

		/** Constructs a list of modules on which the given module depends.
		 *
		 * \param module The module for which the dependency list will be
		 * constructed.
		 * \param loadedOnly If true only the loaded modules will be added to
		 * the list.
		 * \param modules A list to which the modules will be appended.
		 */
		void dependList(const String &module, bool loadedOnly, std::list<String> &modules);

		/** Returns name of the module with given index.
		 */
		const String &getModuleByIndex(size_t index);

		/** Returns index of the module in the list.
		 */
		int getModuleIndex(const String &name);

		/** Returns name of one module that is not loaded and has no unresolved
		 * dependencies.
		 */
		const String &getNextLoadableModule();

		/** Constructs a list of modules that depend on the given module.
		 *
		 * \param module The module for which the list will be constructed.
		 * \param loadedOnly If true only the loaded modules will be added to
		 * the list.
		 * \param modules A list to which the modules will be appended.
		 */
		void modulesThatDepend(const String &module, bool loadedOnly, std::list<String> &modules);

		/** Prepares the module dependencies table.
		 */
		void prepareTable(const String &filename);

		/** Mapping to determine which module has been announced to be
		 * available for loading.
		 */
		std::map<String, bool> announcedAsLoadable;

		/** Table with dependencies between modules. If depTable[i][j] is true
		 * then the module with number j depends on the module with with number
		 * i.
		 */
		bool **depTable;

		/** Mapping to determine which module is loaded.
		 */
		std::map<String, bool> loaded;

		/** List with names of the modules.
		 */
		std::list<String> names;

		/** Path to each module library.
		 */
		std::map<String, String> paths;

	friend class ModuleManager;
};

/** \brief Exception for module errors.
 */
class ModuleException : public Exception
{
	public:
		/** Standard constructor.
		 */
		ModuleException(const String &desc) : Exception(desc) {};

		/** Default destructor.
		 */
		virtual ~ModuleException() {};
};

/** \brief Main class for module management.
 *
 * It allows you to load modules dynamically at runtime. Dependent libraries
 * are loaded automatically.
 *
 * Modules are stored in a directory specified by "modules-directory" in config
 * file. File with dependencies is stored as "modules.dep" in the same
 * directory as modules.
 *
 * This class is a singleton.
 */
class ModuleManager
{
	public:
		/** Loads informations about modules and prepares to load them.
		 */
		ModuleManager();

		/** Unloads all loaded modules.
		 */
		~ModuleManager();

		/** Closes all opened libraries left by unloaded modules.
		 */
		void closeLibraries();

		/** Loads all the available modules. This cannot be done in the
		 * constructor, because some modules may need to reference this class
		 * during their initialization.
		 */
		void initialize();

		/** Loads the module with given name. It must be stored in a directory
		 * specified by configuration file modules-directory/modules.dep as
		 * name-of-module.so.
		 */
		void loadModule(const String &name);

		/** Returns pointer to the instance of this class.
		 */
		static ModuleManager *self() {
			return moduleManager;
		};

		/** Unloads all loaded modules but doesn't actually close libraries.
		 * This is useful becuase even if a module is unloaded something may be
		 * still cleaning up and needs library's code.
		 */
		void unloadModules();

	public:
		/** Signal which is emitted after all modules were initialized. It is
		 * deleted after emition.
		 */
		Signal0<void> *initializedSignal;

	private:
		/** All modules and their dependencies.
		 */
		ModuleDependencies *deps;

		/** List of libraries that are opened without loaded module.
		 */
		std::list<void *> librariesToClose;

		/** Holds pointer to the instance of this class.
		 */
		static ModuleManager *moduleManager;

		/** List of loaded modules.
		 */
		std::list<Module *> modules;
};

}; // namespace Core

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
