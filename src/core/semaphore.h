/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_core_semaphore_h
#define _tairon_core_semaphore_h

#include <semaphore.h>

namespace Tairon
{

namespace Core
{

/** \brief Class providing semaphore synchronization primitive.
 *
 * Semaphores are counters for resources shared between threads. The basic
 * operations on semaphores are: increment the counter atomically, and wait
 * until the counter is non-null and decrement it atomically.
 */
class Semaphore
{
	public:
		/** Initializes the semaphore.
		 *
		 * \param value Initial value of the semaphore.
		 */
		Semaphore(unsigned int value);

		/** Destroys the semaphore and frees all allocated resources. There
		 * must not be any thread waiting for the semaphore.
		 */
		~Semaphore();

		/** Atomically increases the count of the semaphore. This method never
		 * blocks.
		 */
		void post() {
			sem_post(&s);
		};

		/** This method shall lock semaphore as in the wait() method. However,
		 * if the semaphore cannot be locked without waiting for another thread
		 * to unlock the semaphore by calling the post method, this wait shall
		 * be terminated when the specified timeout expires and this method
		 * then returns false.
		 *
		 * \param seconds Number of secons of timeout.
		 * \param nanoseconds Number of nanoseconds of timeout.
		 * \param absolute Wheter the timeout should be relative to the current
		 * time.
		 */
		bool timedWait(time_t seconds, long nanoseconds, bool absolute = false);

		/** Suspends the calling thread until the semaphore has non-zero count.
		 * It then atomically decreases the semaphore count.
		 */
		void wait() {
			sem_wait(&s);
		};

	private:
		/** Semaphore handler.
		 */
		sem_t s;
};

}; // namespace Core

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
