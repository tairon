/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_core_threadmanager_h
#define _tairon_core_threadmanager_h

#include <map>
#include <pthread.h>

#include "string.h"

namespace Tairon
{

namespace Core
{

class Mutex;
class Thread;

/** \brief Holds informations about all created threads.
 *
 * This class is a singleton.
 */
class ThreadManager
{
	public:
		/** Initializes the manager.
		 */
		ThreadManager();

		/** Destroys the manager.
		 */
		~ThreadManager();

		/** Returns the thread object identified by its ID.
		 */
		Thread *getThreadByID(pthread_t id);

		/** Returns the thread object identified by its name.
		 */
		Thread *getThreadByName(const String &name);

		/** Returns pointer to the instance of this class.
		 */
		static ThreadManager *self() {
			return threadManager;
		};

		/** Sets main program's thread.
		 */
		void setMainThread(Thread *thread);

	private:
		/** Registers a new thread.
		 *
		 * \param name Name of the new thread.
		 * \param thread Thread object to register.
		 */
		void registerThread(const String &name, Thread *thread);

		/** Registers an ID of a thread.
		 */
		void registerThreadID(pthread_t id, Thread *thread);

		/** Unregisters a thread.
		 *
		 * \param name Name of the thread to unregister.
		 */
		void unregisterThread(const String &name);

		/** Unregisters an ID.
		 */
		void unregisterThreadID(pthread_t id);

	private:
		/** Main program's thread.
		 */
		Thread *mainThread;

		/** Mapping from an ID to the thread.
		 */
		std::map<pthread_t, Thread *> threadIDs;

		/** Mutex protecting access to the threadIDs.
		 */
		Mutex *threadIDsMutex;

		/** Holds pointer to the instance of this class.
		 */
		static ThreadManager *threadManager;

		/** Mapping from a name to the thread.
		 */
		std::map<String, Thread *> threadNames;

		/** Mutex protecting access to the threadNames.
		 */
		Mutex *threadNamesMutex;

	friend class Thread;
};

}; // namespace Core

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
