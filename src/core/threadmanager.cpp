/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include "threadmanager.h"

#include "mutex.h"
#include "thread.h"

namespace Tairon
{

namespace Core
{

ThreadManager *ThreadManager::threadManager = 0;

/* {{{ ThreadManager::ThreadManager() */
ThreadManager::ThreadManager() : mainThread(0)
{
	threadIDsMutex = new Mutex();
	threadNamesMutex = new Mutex();

	threadManager = this;
}
/* }}} */

/* {{{ ThreadManager::~ThreadManager() */
ThreadManager::~ThreadManager()
{
	if (mainThread)
		mainThread->finished();

	delete threadNamesMutex;
	delete threadIDsMutex;

	threadManager = 0;
}
/* }}} */

/* {{{ ThreadManager::getThreadByID(pthread_t) */
Thread *ThreadManager::getThreadByID(pthread_t id)
{
	Thread *ret = 0;
	threadIDsMutex->lock();
	if (threadIDs.count(id))
		ret = threadIDs[id];
	threadIDsMutex->unlock();
	return ret;
}
/* }}} */

/* {{{ ThreadManager::getThreadByName(const String &) */
Thread *ThreadManager::getThreadByName(const String &name)
{
	Thread *ret = 0;
	threadNamesMutex->lock();
	if (threadNames.count(name))
		ret = threadNames[name];
	threadNamesMutex->unlock();
	return ret;
}
/* }}} */

/* {{{ ThreadManager::registerThread(const String &, Thread *) */
void ThreadManager::registerThread(const String &name, Thread *thread)
{
	threadNamesMutex->lock();
	threadNames[name] = thread;
	threadNamesMutex->unlock();
}
/* }}} */

/* {{{ ThreadManager::registerThreadID(pthread_t, Thread *) */
void ThreadManager::registerThreadID(pthread_t id, Thread *thread)
{
	threadIDsMutex->lock();
	threadIDs[id] = thread;
	threadIDsMutex->unlock();
}
/* }}} */

/* {{{ ThreadManager::setMainThread(Thread *) */
void ThreadManager::setMainThread(Thread *thread)
{
	mainThread = thread;
	mainThread->started();
}
/* }}} */

/* {{{ ThreadManager::unregisterThread(const String &) */
void ThreadManager::unregisterThread(const String &name)
{
	threadNamesMutex->lock();
	threadNames.erase(name);
	threadNamesMutex->unlock();
}
/* }}} */

/* {{{ ThreadManager::unregisterThreadID(pthread_t) */
void ThreadManager::unregisterThreadID(pthread_t id)
{
	threadIDsMutex->lock();
	threadIDs.erase(id);
	threadIDsMutex->unlock();
}
/* }}} */

}; // namespace Core

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
