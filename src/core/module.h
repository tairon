/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_core_module_h
#define _tairon_core_module_h

#include "string.h"

// Every module must use this macro exactly once. It creates helper function
// that is used to create module's factory class which is then used to
// communicate with the module.
#define EXPORT_MODULE(moduleName, moduleClass) \
	extern "C" { void *init_##moduleName() { return new moduleClass; } };

namespace Tairon
{

namespace Core
{

class ModuleManager;

/** \brief Base class for accessing module's functionality.
 *
 * Reimplement this class and use EXPORT_MODULE macro to create new module. It
 * should provide interface to the module's functions.
 */
class Module
{
	public:
		/** Prepares everything that the module needs. Don't create Module
		 * objects directly. Instead use ModuleManager.
		 */
		Module();

		/** Frees everything the module has allocated and should return into
		 * the state before loading the module (if possible).
		 */
		virtual ~Module();

	private:
		/** Pointer to the library handle - required for dl_close.
		 */
		void *library;

		/** Name of the module.
		 */
		String name;

	friend class ModuleManager;
};

}; // namespace Core

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
