/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include <cctype>
#include <sstream>

#include "string.h"

namespace Tairon {

namespace Core {

String String::null;

/* {{{ String::lstrip() */
String &String::lstrip()
{
	size_t pos = 0;
	while (isspace((*this)[pos]) && (pos < length()))
		++pos;
	erase(0, pos);

	return *this;
}
/* }}} */

/* {{{ String::number(double) */
String String::number(double num)
{
	std::stringstream str;
	str << num;
	return str.str();
}
/* }}} */

/* {{{ String::number(int32_t) */
String String::number(int32_t num)
{
	std::stringstream str;
	str << num;
	return str.str();
}
/* }}} */

/* {{{ String::number(uint32_t) */
String String::number(uint32_t num)
{
	std::stringstream str;
	str << num;
	return str.str();
}
/* }}} */

/* {{{ String::number(int64_t) */
String String::number(int64_t num)
{
	std::stringstream str;
	str << num;
	return str.str();
}
/* }}} */

/* {{{ String::number(uint64_t) */
String String::number(uint64_t num)
{
	std::stringstream str;
	str << num;
	return str.str();
}
/* }}} */

/* {{{ String::readLine() */
String String::readLine()
{
	size_t n = find('\n');
	if (n == npos)
		return String();

	String ret = substr(0, n);
	if (ret.length() && (ret[ret.length() - 1] == '\r'))
		ret.resize(ret.length() - 1);
	erase(0, n + 1);
	return ret;
}
/* }}} */

/* {{{ String::rstrip() */
String &String::rstrip()
{
	size_t pos = length() - 1;
	while (isspace((*this)[pos]) && (pos != npos))
		--pos;
	erase(pos + 1);

	return *this;
}
/* }}} */

/* {{{ String::strip() */
String &String::strip()
{
	return lstrip().rstrip();
}
/* }}} */

}; // namespace Core

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
