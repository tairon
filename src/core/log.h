/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_core_log_h
#define _tairon_core_log_h

#include <cstdio>

#ifndef LOGLEVEL
#define LOGLEVEL 0
#endif

#if LOGLEVEL >= 0
#define ERROR(x) printf("Error (" __FILE__ ":%d) %s\n", __LINE__, x)
#else
#define ERROR(x) do { } while (0)
#endif

#if LOGLEVEL >= 1
#define WARNING(x) printf("Warning (" __FILE__ ":%d) %s\n", __LINE__, x)
#else
#define WARNING(x) do { } while (0)
#endif

#if LOGLEVEL >= 2
#define INFO(x) printf("Info (" __FILE__ ":%d) %s\n", __LINE__, x)
#else
#define INFO(x) do { } while (0)
#endif

#if LOGLEVEL >= 3
#define DEBUG(x) printf("Debug (" __FILE__ ":%d) %s\n", __LINE__, x)
#else
#define DEBUG(x) do { } while (0)
#endif

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
