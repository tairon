/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_core_mutex_h
#define _tairon_core_mutex_h

#include <pthread.h>

namespace Tairon
{

namespace Core
{

/** \brief This class provides mutex functionality.
 *
 * A mutex is a MUTual EXclusion device, and is useful for protecting shared
 * data structures from concurrent modification, and implementing critical
 * sections.
 *
 * A mutex has two possible states: unlocked (not owned by any thread), and
 * locked (owned by one thread). A mutex can never be owned by two different
 * threads simultaneously. A thread attempting to lock a mutex that is already
 * locked by another thread is suspended until owning thread unlocks the mutex
 * first.
 */
class Mutex
{
	public:
		/** Initializes the mutex object.
		 */
		Mutex();

		/** Destroys this mutex object. The mutex must not be locked.
		 */
		~Mutex();

		/** Locks this mutex. If the mutex is currently unlocked, it becomes
		 * locked and owned by the calling thread and this method returns
		 * immediately. If the mutex is already locked then this method
		 * suspends calling thread until the mutex is unlocked.
		 */
		void lock();

		/** This method behaves like lock() method, except that it does not
		 * block the calling thread if the mutex is already locked. This method
		 * returns true if the mutex can be locked, otherwise returns false.
		 */
		bool tryLock();

		/** Unlocks this mutex.
		 */
		void unlock();

	private:
		/** ID of this mutex.
		 */
		pthread_mutex_t id;
};

}; // namespace Core

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
