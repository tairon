/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_core_config_h
#define _tairon_core_config_h

#include <map>

#include "string.h"

namespace Tairon
{

namespace Core
{

/** \brief Class that holds configuration.
 *
 * It loads a configuration file with simple key-value based structure and then
 * provides loaded informations to the rest of the program. This class is a
 * singleton.
 */
class Config
{
	public:
		/** Loads a configuration file. It is an XML file with "config" as the
		 * root element's name. Names of root's direct children are used as
		 * keys and their texts are treated as values.
		 * 
		 * \param filename Path to the configuration file. If it is pure
		 * filename without any directory then it is searched in following
		 * paths: . (current directory), /etc, /usr/etc and /usr/local/etc.
		 */
		Config(const String &filename);

		/** Standard destructor.
		 */
		~Config();

		/** Returns the key's value. Default value is returned when the key
		 * doesn't exist.
		 */
		const String &operator[](const String &key);

		/** Returns pointer to the instance of this class.
		 */
		static Config *self() {
			return config;
		};

	private:
		/** Loads the configuration file and stores data from it into the
		 * internal dictionary.
		 */
		void loadConfigFile(const String &filename);

		/** Holds pointer to the instance of this class.
		 */
		static Config *config;

		/** Dictionary holding configuration.
		 */
		std::map<String, String> data;
};

}; // namespace Core

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
