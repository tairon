/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include <errno.h>

#include "mutex.h"

namespace Tairon
{

namespace Core
{

/* {{{ Mutex::Mutex() */
Mutex::Mutex()
{
	pthread_mutex_init(&id, 0);
}
/* }}} */

/* {{{ Mutex::~Mutex() */
Mutex::~Mutex()
{
	pthread_mutex_destroy(&id);
}
/* }}} */

/* {{{ Mutex::lock() */
void Mutex::lock()
{
	pthread_mutex_lock(&id);
}
/* }}} */

/* {{{ Mutex::tryLock() */
bool Mutex::tryLock()
{
	if (pthread_mutex_trylock(&id) == EBUSY)
		return false;
	return true;
}
/* }}} */

/* {{{ Mutex::unlock() */
void Mutex::unlock()
{
	pthread_mutex_unlock(&id);
}
/* }}} */

}; // namespace Core

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
