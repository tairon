/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

// This implementation is heavily inspired by module manager written by Tomas
// Mecir for tnMUD (http://tnmud.sourceforge.net).

#include <dlfcn.h>

#include "modulemanager.h"

#include "tinyxml/tinyxml.h"
#include "config.h"
#include "exceptions.h"
#include "log.h"
#include "module.h"

namespace Tairon
{

namespace Core
{

ModuleManager *ModuleManager::moduleManager = 0;


/* {{{ ModuleDependencies::ModuleDependencies(const String &) */
ModuleDependencies::ModuleDependencies(const String &filename)
{
	depTable = 0;
	prepareTable(filename);
}
/* }}} */

/* {{{ ModuleDependencies::~ModuleDependencies() */
ModuleDependencies::~ModuleDependencies()
{
	for (size_t i = 0; i < names.size(); ++i)
		delete [] depTable[i];
	delete [] depTable;
	depTable = 0;
}
/* }}} */

/* {{{ ModuleDependencies::dependList(const String &, bool, std::list<String> &) */
void ModuleDependencies::dependList(const String &module, bool loadedOnly, std::list<String> &modules)
{
	int index = getModuleIndex(module);
	if (index == -1)
		return;

	// construct the dependency list
	size_t count = names.size();
	for (size_t i = 0; i < count; ++i)
		if (depTable[i][index]) { // <module> depends on module with index i
			const String &name = getModuleByIndex(i);
			if (!loadedOnly || loaded[name])
				modules.push_back(name);
		}
}
/* }}} */

/* {{{ ModuleDependencies::getModuleByIndex(size_t) */
const String &ModuleDependencies::getModuleByIndex(size_t index)
{
	std::list<String>::iterator it = names.begin();
	for (size_t i = 0; i < index; ++i)
		++it;
	return *it;
}
/* }}} */

/* {{{ ModuleDependencies::getModuleIndex(const String &) */
int ModuleDependencies::getModuleIndex(const String &name)
{
	int ret = 0;
	std::list<String>::iterator it;
	for (it = names.begin(); it != names.end(); ++it) {
		if (*it == name)
			break;
		++ret;
	}
	if (it == names.end()) // module has not been found
		return -1;
	return ret;
}
/* }}} */

/* {{{ ModuleDependencies::getNextLoadableModule() */
const String &ModuleDependencies::getNextLoadableModule()
{
	for (std::list<String>::iterator it = names.begin(); it != names.end(); ++it) {
		std::list<String> l1;
		std::list<String> l2;
		dependList(*it, false, l1);
		dependList(*it, true, l2);
		int count = l1.size() - l2.size(); // all not loaded modules that the current one depends on
		if ((count == 0) && !announcedAsLoadable[*it]) {
			// no dependencies, this module has not been announced yet
			// (prevents endless loop for modules that cannot be loaded for any
			// reason)
			announcedAsLoadable[*it] = true;
			return *it;
		}
	}

	// nothing left to load; either everything was loaded or only modules with
	// circular dependencies remain
	return String::null;
}
/* }}} */

/* {{{ ModuleDependencies::modulesThatDepend(const String &, bool, std::list<String> &) */
void ModuleDependencies::modulesThatDepend(const String &module, bool loadedOnly, std::list<String> &modules)
{
	int index = getModuleIndex(module);
	if (index == -1)
		return;

	// constuct the dependency list
	size_t count = names.size();
	for (size_t i = 0; i < count; ++i)
		if (depTable[index][i]) { // module with index i depends on this module
			const String &name = getModuleByIndex(i);
			if (!loadedOnly || loaded[name])
				modules.push_back(name);
		}
}
/* }}} */

/* {{{ ModuleDependencies::prepareTable(const String &) */
void ModuleDependencies::prepareTable(const String &filename)
{
	// current implementation MUST NOT be called more than once!

	// load file
	TiXmlDocument doc;
	if (!doc.LoadFile(filename))
		throw XMLException("Cannot load the dependency file " + filename);

	TiXmlNode *root = doc.RootElement();
	if (!root)
		throw XMLException("No root element in the dependency file " + filename);

	if (root->ValueStr() != "modules")
		throw XMLException("Name of the root node mismatches in the dependency file " + filename);

	TiXmlElement *element;
	for (TiXmlNode *node = root->FirstChild(); node; node = node->NextSibling()) {
		if ((node->Type() != TiXmlNode::ELEMENT) || (node->ValueStr() != "module"))
			continue;

		element = node->ToElement();

		const char *modName = element->Attribute("name");
		if (!modName)
			continue;
		String name(modName);

		const char *modDir = element->Attribute("directory");
		String dir;
		if (modDir)
			dir = modDir;
		else
			dir = "";

		// ok, set information about module
		names.push_back(modName);
		paths[modName] = (*Config::self())["modules-directory"] + String(dir.length() ? "/" : "") + dir;
		loaded[modName] = false;

		const char *modLoad = element->Attribute("load");
		if (modLoad)
			if (strcmp(modLoad, "no")) // if it is not "no"
				announcedAsLoadable[modName] = false;
			else // don't load this module
				announcedAsLoadable[modName] = true;
		else
			announcedAsLoadable[modName] = false;
	}

	// we have informations about modules here, prepare table
	size_t count = names.size();
	depTable = new bool *[count];
	for (size_t i = 0; i < count; ++i) {
		depTable[i] = new bool[count];
		for (size_t j = 0; j < count; ++j) // no dependencies by default
			depTable[i][j] = false;
	}

	// read dependency list
	int modIndex = -1;
	for (TiXmlNode *node = root->FirstChild(); node; node = node->NextSibling()) {
		if ((node->Type() != TiXmlNode::ELEMENT) || (node->ValueStr() != "module"))
			continue;

		element = node->ToElement();

		const char *modName = element->Attribute("name");
		if (!modName)
			continue;

		++modIndex;
		for (TiXmlNode *depNode = node->FirstChild(); depNode; depNode = depNode->NextSibling()) {
			if ((depNode->Type() != TiXmlNode::ELEMENT) || (depNode->ValueStr() != "depend"))
				continue;

			// modDepOn holds name of the module the current module depends on
			const char *modDepOn = depNode->ToElement()->GetText();
			if (!modDepOn)
				continue;

			int depIndex = -1;
			int di = -1;
			for (std::list<String>::iterator it = names.begin(); it != names.end(); ++it) {
				++di;
				if (*it == modDepOn) { // the module's name has been found
					depIndex = di;
					break;
				}
			}

			if ((modIndex == -1) || (depIndex == -1)) // something is wrong
				throw ModuleException(String("Error in dependency file at dependency ") + modName + " -> " + modDepOn);
			else
				depTable[depIndex][modIndex] = true;
		}
	}

	INFO((const char *) String("Successfuly loaded information about " + String::number(count) + " modules"));

	// find circular dependency in the table

	/* To do this we use topological search (the table is treated as an
	 * oriented graph, with modules being vertices and dependencies being
	 * arrows (orientation I -> J if the module J depends on the module I),
	 * thus no arrows end in modules that have no dependencies):
	 *
	 * We simulate loading of modules, reducing number of unsatisfied
	 * dependencies for each module when appropriate. When choosing which
	 * module to load, we pick the one that has no unsatisfied dependencies. If
	 * there is none, then there certainly is a cycle with all modules with
	 * non-zero still-unsatisfied dependencies being members of at least one
	 * such cycle (see graph algorithms - topological search for more detailed
	 * explanation and a proof of correctness)
	 */

	size_t notLoaded = count;
	bool *loaded = new bool[count]; // is the module treated as loaded?
	int *depCount = new int[count]; // number of currently unsatisfied dependencies for module i

	for (size_t i = 0; i < count; ++i) {
		loaded[i] = false;
		depCount[i] = 0;
	}

	// count number of dependencies
	for (size_t i = 0; i < count; ++i)
		for (size_t j = 0; j < count; ++j)
			if (depTable[j][i]) // does module i depend on moduje j?
				++depCount[i];

	bool changed;
	do { // load as long as possible
		changed = false;
		// simulate loading of all modules that can be loaded
		for (size_t i = 0; i < count; ++i)
			if (!loaded[i] && (depCount[i] == 0)) { // we can load this one
				loaded[i] = true;
				changed = true;
				--notLoaded;
				for (size_t j = 0; j < count; ++j) // go through all modules
					if (depTable[i][j]) // if that module depends on the current one
						--depCount[j]; // reduce number of dependencies
			}
	} while (changed && (notLoaded > 0));

	// Now if the notLoaded > 0 then it is not possible to load some modules
	// because of circular dependency.

	if (notLoaded) {
		WARNING((const char *) String("Because of circular dependencies, " + String::number(notLoaded) + " out of " + String::number(count) + " modules will not be available"));
	}

	delete [] depCount;
	delete [] loaded;

	// complete the table, making "be-dependent" relation transitive

	// Floyd-Warshall's algorithm, a bit modified - path values are always 1;
	// there is added changed cycle to be sure that the modifications didn't
	// corrupt the algorithm.
	do {
		changed = false;
		for (size_t i = 0; i < count; ++i)
			for (size_t j = 0; j < count; ++j)
				for (size_t k = 0; k < count; ++k)
					if (depTable[i][j] && depTable[j][k] && !depTable[i][k]) {
						// J depends on I and K depends on J => K depends on I
						depTable[i][k] = true;
						changed = true;
					}
	} while (changed);
}
/* }}} */


/* {{{ ModuleManager::ModuleManager() */
ModuleManager::ModuleManager()
{
	if (ModuleManager::moduleManager)
		throw SingletonException("Cannot make another instance of Tairon::Core::ModuleManager class");

	INFO("ModuleManager starting");

	deps = new ModuleDependencies((*Config::self())["modules-directory"] + "/modules.dep");

	initializedSignal = new Signal0<void>();

	ModuleManager::moduleManager = this;
}
/* }}} */

/* {{{ ModuleManager::~ModuleManager() */
ModuleManager::~ModuleManager()
{
	// TODO: add locking

	INFO("Module manager quitting, unloading all modules");

	unloadModules();

	delete deps;

	// just in case that initialize() method has not been called
	delete initializedSignal;

	ModuleManager::moduleManager = 0;
}
/* }}} */

/* {{{ ModuleManager::closeLibraries() */
void ModuleManager::closeLibraries()
{
	for (std::list<void *>::const_iterator it = librariesToClose.begin(); it != librariesToClose.end(); ++it)
		dlclose(*it);
	librariesToClose.clear();
}
/* }}} */

/* {{{ ModuleManager::initialize() */
void ModuleManager::initialize()
{
	INFO("Loading all available modules");

	// We go through the list of loadable modules. There is no problem if a
	// module cannot be loaded because
	// ModuleDependencies::getNextLoadableModule() returns each module at most
	// once.
	String name;
	while ((name = deps->getNextLoadableModule()) != String::null)
		loadModule(name);

	INFO("Modules loaded, ModuleManager ready");

	initializedSignal->emit();
	delete initializedSignal;
	initializedSignal = 0;
}
/* }}} */

/* {{{ ModuleManager::loadModule(const String &) */
void ModuleManager::loadModule(const String &name)
{
	INFO((const char *) String("Loading module " + name));
	String libFilename = deps->paths[name] + "/" + name + ".so";

	// check access
	if (access(libFilename, R_OK))
		throw IOException("Cannot open module file " + libFilename);

	if (deps->loaded[name])
		throw ModuleException("Module " + name + " is already loaded");

	std::list<String> l1;
	std::list<String> l2;
	deps->dependList(name, false, l1); // all dependencies
	deps->dependList(name, true, l2); // loaded dependencies;
	if (l1.size() > l2.size()) // there are unresolved dependencies
		throw ModuleException("Unresolved dependencies for module " + name);

	// initialize the library via dlopen
	void *lib = dlopen(libFilename, RTLD_NOW | RTLD_GLOBAL);
	if (!lib) {
		ERROR(dlerror());
		throw ModuleException("Cannot load module " + name);
	}

	// init function
	void *(*initFn)() = (void *(*)()) dlsym(lib, String("init_" + name));
	Module *module = 0;
	if (initFn)
		module = (Module *) initFn(); // initialize the module

	if (module) {
		module->library = lib; // remember handler
		module->name = name; // set name
		modules.push_back(module); // add to the list of loaded modules
		deps->loaded[name] = true; // mark as loaded
	} else {
		dlclose(lib);
		throw ModuleException("Cannot initialize module " + name);
	}
}
/* }}} */

/* {{{ ModuleManager::unloadModules() */
void ModuleManager::unloadModules()
{
	// Currently the modules are unloading in the reverse order than they has
	// been loaded - ti should be sufficient for correct dependencies

	for (std::list<Module *>::reverse_iterator it = modules.rbegin(); it != modules.rend(); ++it) {
		Module *module = *it;
		INFO((const char *) String("Unloading " + (*it)->name));

		void *lib = module->library;
		delete module; // free main module's object - it must clean all other module's resources

		librariesToClose.push_back(lib);
	}

	modules.clear();
}
/* }}} */

}; // namespace Core

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
