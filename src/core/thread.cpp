/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include <pthread.h>

#include "thread.h"

#include "signals.h"
#include "threadmanager.h"

namespace Tairon
{

namespace Core
{

/* {{{ startFunction(void *) */
void *startFunction(void *data)
{
	((Thread *) data)->started();
	return ((Thread *) data)->run();
	((Thread *) data)->finished();
}
/* }}} */

/* {{{ Thread::Thread(const String &) */
Thread::Thread(const String &n) : name(n)
{
	callersMutex = new Mutex();
	callersSemaphore = new Semaphore(0);
	ThreadManager::self()->registerThread(name, this);
}
/* }}} */

/* {{{ Thread::~Thread() */
Thread::~Thread()
{
	ThreadManager::self()->unregisterThread(name);
	delete callersSemaphore;
	delete callersMutex;
}
/* }}} */

/* {{{ Thread::callFunctors() */
void Thread::callFunctors()
{
	ThreadFunctorCaller *caller;

	callersMutex->lock();
	while (callers.size()) {
		caller = callers.front();
		callers.pop_front();
		callersMutex->unlock();
		// Just decrement the semaphore. This should never block.
		callersSemaphore->wait();
		(*caller)();
		callersMutex->lock();
	}
	callersMutex->unlock();
}
/* }}} */

/* {{{ Thread::current() */
Thread *Thread::current()
{
	return ThreadManager::self()->getThreadByID(pthread_self());
}
/* }}} */

/* {{{ Thread::finished() */
void Thread::finished()
{
	ThreadManager::self()->unregisterThreadID(pthread_self());
}
/* }}} */

/* {{{ Thread::join() */
void *Thread::join()
{
	void *ret;
	int err = pthread_join(tid, &ret);
	if (err)
		throw ThreadException("Cannot join thread (" + String::number(err) + ")");
	return ret;
}
/* }}} */

/* {{{ Thread::start() */
void Thread::start()
{
	if (pthread_create(&tid, 0, startFunction, this))
		throw ThreadException("Cannot create thread");
}
/* }}} */

/* {{{ Thread::started() */
void Thread::started()
{
	ThreadManager::self()->registerThreadID(pthread_self(), this);
}
/* }}} */

/* {{{ Thread::waitAndCallFunctors(time_t, unsigned long) */
void Thread::waitAndCallFunctors(time_t seconds, unsigned long nanoseconds)
{
	if (callersSemaphore->timedWait(seconds, nanoseconds, false)) {
		// return decremented semaphore to its original value
		callersSemaphore->post();
		callFunctors();
	}
}
/* }}} */

}; // namespace Core

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
