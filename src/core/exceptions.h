/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_core_exceptions_h
#define _tairon_core_exceptions_h

#include "string.h"

namespace Tairon
{

namespace Core
{

/** \brief Basic class for all Tairon-related exceptions.
 */
class Exception
{
	public:
		/** Constructor that takes as an argument only description of the exception.
		 */
		Exception(const String &desc);

		/** Does nothing.
		 */
		virtual ~Exception() {};

		/** Cast operator.
		 */
		virtual operator const String &() const;

	protected:
		/** Description of the exception.
		 */
		String description;
};

/** \brief Exception for input/output errors.
 */
class IOException : public Exception
{
	public:
		/** Standard constructor.
		 */
		IOException(const String &desc) : Exception(desc) {};

		/** Default destructor.
		 */
		virtual ~IOException() {};
};

/** \brief Exception thrown when a given key is invalid.
 */
class KeyException : public Exception
{
	public:
		/** Standard constructor.
		 */
		KeyException(const String &desc) : Exception(desc) {};

		/** Default destructor.
		 */
		virtual ~KeyException() {};
};

/** \brief Exception for failed singleton checks.
 */
class SingletonException : public Exception
{
	public:
		/** Standard constructor.
		 */
		SingletonException(const String &desc) : Exception(desc) {};

		/** Default destructor.
		 */
		virtual ~SingletonException() {};
};

/** \brief Exception for XML errors.
 */
class XMLException : public Exception
{
	public:
		/** Standard constructor.
		 */
		XMLException(const String &desc) : Exception(desc) {};

		/** Default destructor.
		 */
		virtual ~XMLException() {};
};

}; // namespace Core

}; // namespace Tairon


#endif

// vim: ai sw=4 ts=4 noet fdm=marker
