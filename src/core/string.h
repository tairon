/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_core_string_h
#define _tairon_core_string_h

#include <string>
#include <stdint.h>

namespace Tairon
{

namespace Core
{

/** \brief Just wrapper class around std::string.
 *
 * It is here because of preparation to later optimization with implicitly
 * shared data.
 */
class String : public std::string
{
	public:
		String() : std::string() {} ;

		String(const std::string &str) : std::string(str) {};

		String(const String &str, size_t pos, size_t count = npos) : std::string(str, pos, count) {};

		String(const char *str, size_t size) : std::string(str, size) {};

		String(const char *str) : std::string(str) {};

		bool canReadLine() {
			return contains('\n');
		};

		/** Returns true if the string contains given one.
		 */
		bool contains(const String &what) const {
			return find(what) != npos;
		}

		/** Returns true if the string contains given character.
		 */
		bool contains(char what) const {
			return find(what) != npos;
		}

		/** Removes all leading whitespaces. Returns reference to *this.
		 */
		String &lstrip();

		/** Converts given number into a string.
		 */
		static String number(double num);

		/** Converts given number into a string.
		 */
		static String number(int32_t num);

		/** Converts given number into a string.
		 */
		static String number(uint32_t num);

		/** Converts given number into a string.
		 */
		static String number(int64_t num);

		/** Converts given number into a string.
		 */
		static String number(uint64_t num);

		/** Cast operator.
		 */
		operator const char *() const {
			return c_str();
		}

		/** Returns string containing characters up to first newline character.
		 * The beginning of this string with newline character is removed. If
		 * there is no newline character then this method returns empty string.
		 */
		String readLine();

		/** Removes all trailing whitespaces from the end of the string.
		 * Returns reference to *this.
		 */
		String &rstrip();

		/** Removes all whitespaces from the beginning and end of the string.
		 * Returns reference to *this.
		 */
		String &strip();

	public:
		static String null;
};

}; // namespace Core

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
