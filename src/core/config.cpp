/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include "config.h"

#include "tinyxml/tinyxml.h"
#include "exceptions.h"

static const char *configDirs[] = {
	"./",
	"/etc/",
	"/usr/etc/",
	"/usr/local/etc/",
	0
};

namespace Tairon
{

namespace Core
{

Config *Config::config = 0;

/* {{{ Config::Config(const String &) */
Config::Config(const String &filename)
{
	if (Config::config)
		throw SingletonException("Cannot make another instance of Tairon::Core::Config class");

	if (filename.contains('/')) { // contains directory
		if (access(filename, R_OK) == 0)
			loadConfigFile(filename);
		else
			throw IOException("Cannot access the configuration file " + filename);
	} else { // just filename
		unsigned int i = 0;
		while (configDirs[i]) {
			String path = String(configDirs[i])	+ filename;
			if (access(path, R_OK) == 0) {
				loadConfigFile(path);
				break;
			}
			++i;
		}
		if (!configDirs[i])
			throw IOException("Cannot access any configuration file");
	}

	Config::config = this;
}
/* }}} */

/* {{{ Config::~Config() */
Config::~Config()
{
	Config::config = 0;
}
/* }}} */

/* {{{ Config::loadConfigFile(const String &) */
void Config::loadConfigFile(const String &filename)
{
	TiXmlDocument document;

	if (!document.LoadFile(filename))
		throw XMLException("Cannot load the configuration file " + filename);

	TiXmlElement *root = document.RootElement();
	if (!root)
		throw XMLException("No root element in the configuration file " + filename);

	if (root->ValueStr() != "config")
		throw XMLException("Name of the root node mismatches in the configuration file " + filename);

	// we have correct root node of config file here, load it

	TiXmlElement *element;
	for (TiXmlNode *node = root->FirstChild(); node; node = node->NextSibling()) {
		if (node->Type() != TiXmlNode::ELEMENT)
			continue;
		element = node->ToElement();
		data[element->ValueStr()] = element->GetText();
	}
}
/* }}} */

/* {{{ Config::operator[](const String &) */
const String &Config::operator[](const String &key)
{
	if (data.count(key))
		return data[key];
	throw KeyException("Invalid configuration key (" + key + ")");
}
/* }}} */

}; // namespace Core

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
