/***************************************************************************
 *                                                                         *
 *   Copyright (C) <year>  <author>                                        *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include <errno.h>
#include <time.h>

#include "semaphore.h"

namespace Tairon
{

namespace Core
{

/* {{{ Semaphore::Semaphore(unsigned int) */
Semaphore::Semaphore(unsigned int value)
{
	sem_init(&s, 0, value);
}
/* }}} */

/* {{{ Semaphore::~Semaphore */
Semaphore::~Semaphore()
{
	sem_destroy(&s);
}
/* }}} */

/* {{{ Semaphore::timedWait(time_t, long, bool) */
bool Semaphore::timedWait(time_t seconds, long nanoseconds, bool absolute)
{
	struct timespec t;
	if (absolute) {
		t.tv_sec = seconds;
		t.tv_nsec = nanoseconds;
	} else {
		struct timespec current;
		clock_gettime(CLOCK_REALTIME, &current);
		t.tv_sec = current.tv_sec + seconds + (current.tv_nsec + nanoseconds) / 1000000000;
		t.tv_nsec = (current.tv_nsec + nanoseconds) % 1000000000;
	}

	if (sem_timedwait(&s, &t) != 0)
			return false;
	return true;
}
/* }}} */

}; // namespace Core

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
