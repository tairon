/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_limit_limiter_h
#define _tairon_net_limit_limiter_h

#include <list>

#include <tairon/core/signals.h>

namespace Tairon
{

namespace Net
{

/** \brief Limiter is used for limiting network bandwidth.
 */
class Limiter
{
	public:
		/** Constructs a Limiter object.
		 *
		 * \param r Maximum number of bytes to by transferred per second.
		 * \param p Parent of this limiter. Transfer rate of this limiter won't
		 * exceed its parent's rate. If no parent is specified then this
		 * limiter notices its child about free bandwidth to use. An instance
		 * of Tairon::Net::LimitManager has to be created before Limiter can be
		 * used.
		 */
		Limiter(size_t r, Limiter *p = 0);

		/** Destroys the object.
		 */
		~Limiter();

		/** Returns maximum number of bytes that can be transferred.
		 */
		size_t getAllowedSize(size_t max);

		/** Removes a functor from queue of functors waiting for free transfer.
		 */
		void removeFromQueue(Tairon::Core::Functor0<void> *f);

		/** Sets number of transferred bytes to zero and calls waiting functors.
		 */
		void reset();

		/** Informs this Limiter about transferred data.
		 */
		void transferred(size_t s);

		/** Someone wants to be notified when there is a possibility to transfer
		 * more data.
		 *
		 * \param f Functor that will be called when there are data to read.
		 */
		void wantMore(Tairon::Core::Functor0<void> *f);

	private:
		/** Parent of this limiter.
		 */
		Limiter *parent;

		/** Maximum number of bytes to be transferred per second.
		 */
		size_t rate;

		/** Number of bytes that can be transferred.
		 */
		size_t rest;

		/** List of functors that will be called when a transfer can be done.
		 */
		std::list<Tairon::Core::Functor0<void> *> waiting;

		/** Functor for calling wantMore() method.
		 */
		Tairon::Core::Functor0<void> *wantMoreFunctor;
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
