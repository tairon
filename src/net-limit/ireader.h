/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_limit_ireader_h
#define _tairon_net_limit_ireader_h

#include "reader.h"

namespace Tairon
{

namespace Net
{

/** \brief This class reads data from a socket into the internal buffer.
 */
class IReader : public Reader
{
	public:
		/** Creates an IReader object.
		 *
		 * \param len Length of the internal buffer.
		 * \param s Socket used for reading.
		 * \param l Limiter used for limiting bandwidth.
		 */
		IReader(size_t len, Socket *s, Limiter *l);

		/** Destroys the object.
		 */
		virtual ~IReader();
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
