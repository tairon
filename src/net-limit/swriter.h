/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 Davi Brodsky                                       *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_limit_swriter_h
#define _tairon_net_limit_swriter_h

#include <tairon/core/string.h>

#include "writer.h"

using Tairon::Core::String;

namespace Tairon
{

namespace Net
{

/** \brief This class writes a string to a socket.
 */
class SWriter : public Writer
{
	public:
		/** Creates an SWriter object.
		 *
		 * \param str String to write.
		 * \param s Socket to which the string will be written.
		 * \param l Limiter used for limiting bandwidth.
		 */
		SWriter(const String &str, Socket *s, Limiter *l);

		/** Destroys the object.
		 */
		virtual ~SWriter();

	private:
		/** String to write.
		 */
		String string;
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
