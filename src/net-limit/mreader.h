/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_limit_mreader_h
#define _tairon_net_limit_mreader_h

#include "reader.h"

namespace Tairon
{

namespace Net
{

/** \brief This class reads data from a socket and stores them somewhere in the
 * memory.
 */
class MReader : public Reader
{
	public:
		/** Creates an MReader object.
		 *
		 * \param mem Memory location at which the data will be stored.
		 * \param len Length of the memory buffer.
		 * \param s Socket used for reading.
		 * \param l Limiter used for limiting bandwidth.
		 */
		MReader(char *mem, size_t len, Socket *s, Limiter *l);

		/** Destroys the object.
		 */
		virtual ~MReader();
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
