/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_limit_mwriter_h
#define _tairon_net_limit_mwriter_h

#include "writer.h"

namespace Tairon
{

namespace Net
{

/** \brief This class writes a data from a memory to a socket.
 */
class MWriter : public Writer
{
	public:
		/** Creates an MWriter object.
		 *
		 * \param m Memory location with data to write.
		 * \param len Length of data to write.
		 * \param s Socket to which the data will be written.
		 * \param l Limiter used for limiting bandwidth.
		 */
		MWriter(const char *m, size_t len, Socket *s, Limiter *l);


		/** Destroys the object.
		 */
		virtual ~MWriter();
};

}; // namespace Net

};

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
