/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include "mwriter.h"

namespace Tairon
{

namespace Net
{

/* {{{ MWriter::MWriter(const char *, size_t, Socket *, Limiter *) */
MWriter::MWriter(const char *m, size_t len, Socket *s, Limiter *l) : Writer(s, l)
{
	buffer = m;
	bufSize = len;
}
/* }}} */

/* {{{ MWriter::~MWriter() */
MWriter::~MWriter()
{
}
/* }}} */

}; // namespace Net

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
