/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include "limiter.h"

#include "limitmanager.h"

namespace Tairon
{

namespace Net
{

// One may ask a question why we use a list of functors for clients that want
// to be notified about free bandwidth and not regular signal. It is because
// signal notifies all of its connected slots while we want to notify only
// those clients that can be satisfied.

/* {{{ Limiter::Limiter(size_t) */
Limiter::Limiter(size_t r, Limiter *p) : parent(p), rate(r)
{
	rest = rate;

	wantMoreFunctor = Tairon::Core::methodFunctor(this, &Limiter::reset);
}
/* }}} */

/* {{{ Limiter::~Limiter() */
Limiter::~Limiter()
{
	if (parent)
		parent->removeFromQueue(wantMoreFunctor);
	else
		LimitManager::self()->removeFromQueue(this);

	delete wantMoreFunctor;
}
/* }}} */

/* {{{ Limiter::getAllowedSize(size_t) */
size_t Limiter::getAllowedSize(size_t max)
{
	if (max > rest)
		return rest;
	return max;
}
/* }}} */

/* {{{ Limiter::removeFromQueue(Tairon::Core::Functor0<void> *) */
void Limiter::removeFromQueue(Tairon::Core::Functor0<void> *f)
{
	waiting.remove(f);
}
/* }}} */

/* {{{ Limiter::reset() */
void Limiter::reset()
{
	rest = rate;
	Tairon::Core::Functor0<void> *f;
	while (waiting.size() && (rest > 0)) {
		// we cannot use (*waiting.front())() here because waiting.pop_front()
		// may fail causing crash due to empty queue (which has been emptied by
		// code called by the functor
		f = waiting.front();
		waiting.pop_front();
		(*f)();
	}
}
/* }}} */

/* {{{ Limiter::transferred(size_t) */
void Limiter::transferred(size_t s)
{
	rest -= s;
}
/* }}} */

/* {{{ Limiter::wantMore(Tairon::Core::Functor0<void> *) */
void Limiter::wantMore(Tairon::Core::Functor0<void> *f)
{
	if (parent)
		parent->wantMore(wantMoreFunctor);
	else
		LimitManager::self()->wantMore(this);

	waiting.push_back(f);
}
/* }}} */

}; // namespace Net

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
