/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include <tairon/net/socket.h>

#include "reader.h"

#include "limiter.h"

namespace Tairon
{

namespace Net
{

/* {{{ Reader::Reader(Socket *) */
Reader::Reader(Socket *s, Limiter *l) : limiter(l), offset(0), socket(s)
{
	readFunctor = Tairon::Core::methodFunctor(this, &Reader::read);
}
/* }}} */

/* {{{ Reader::~Reader() */
Reader::~Reader()
{
	limiter->removeFromQueue(readFunctor);
	delete readFunctor;
}
/* }}} */

/* {{{ Reader::read() */
void Reader::read()
{
	size_t count = limiter->getAllowedSize(bufSize - offset);

	if (!count) // no more bytes allowed to read
		limiter->wantMore(readFunctor);
		// it isn't needed to delete socket from polling here because it's
		// been already done
	else {
		try {
			size_t len = socket->readTo(buffer + offset, count);
			if (len) {
				offset += len;
				limiter->transferred(len);
				if (offset == bufSize)
					bufferFullSignal.emit(this);
				else
					dataReadSignal.emit(this, len);

				// it isn't needed to do limiter->wantMore() here because when
				// we read from the socket it's been added to polling so this
				// method will be called anyway
			}
		} catch (const Tairon::Net::SocketException &) { // connection has been closed or something else bad happened
			socket->close();
			errorSignal.emit(this, socket);
		}
	}
}
/* }}} */

/* {{{ Reader::reset() */
void Reader::reset()
{
	offset = 0;
}
/* }}} */

}; // namespace Net

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
