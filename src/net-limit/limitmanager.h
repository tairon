/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_limit_limitmanager_h
#define _tairon_net_limit_limitmanager_h

#include <list>
#include <set>

namespace Tairon
{

namespace Net
{

class Limiter;
class Timer;

/** \brief LimitManager handles list of limiters that are waiting for free
 * bandwidth.
 */
class LimitManager
{
	public:
		/** Creates a LimitManager object.
		 */
		LimitManager();

		/** Destroys the manager.
		 */
		~LimitManager();

		/** Removes a limiter from the list.
		 */
		void removeFromQueue(Limiter *l);

		/** Returns pointer the instance of this class.
		 */
		static LimitManager *self() {
			return limitmanager;
		};

		/** Adds a limiter to the list.
		 */
		void wantMore(Limiter *l);

	private:
		/** Called every second by a timer.
		 */
		void timeout();

	private:
		/** Pointer to the instance of this class.
		 */
		static LimitManager *limitmanager;

		/** Obe second timer.
		 */
		Timer *timer;

		/** List of limiters that are about to be deleted from the queue.
		 */
		std::list<Limiter *> toDelete;

		/** List of waiting limiters.
		 */
		std::set<Limiter *> waiting;
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
