/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include <tairon/net/timer.h>

#include "limitmanager.h"

#include "limiter.h"

namespace Tairon
{

namespace Net
{

LimitManager *LimitManager::limitmanager = 0;

/* {{{ LimitManager::LimitManager() */
LimitManager::LimitManager()
{
	timer = new Timer();
	timer->timeoutSignal.connect(Tairon::Core::threadMethodDFunctor(Tairon::Core::Thread::current(), this, &LimitManager::timeout));
	timer->start(1000); // every second

	limitmanager = this;
}
/* }}} */

/* {{{ LimitManager::~LimitManager() */
LimitManager::~LimitManager()
{
	timer->destroy();
}
/* }}} */

/* {{{ LimitManager::removeFromQueue(Limiter *) */
void LimitManager::removeFromQueue(Limiter *l)
{
	toDelete.push_back(l);
}
/* }}} */

/* {{{ LimitManager::timeout() */
void LimitManager::timeout()
{
	// A crash due to an invalid iterator may be caused if a limiter wants to
	// be removed from the queue while the queue is traversed. This workaround
	// should do the right thing.
	for (std::list<Limiter *>::const_iterator it = toDelete.begin(); it != toDelete.end(); ++it)
		waiting.erase(*it);
	toDelete.clear();

	for (std::set<Limiter *>::const_iterator it = waiting.begin(); it != waiting.end(); ++it)
		(*it)->reset();
}
/* }}} */

/* {{{ LimitManager::wantMore(Limiter *) */
void LimitManager::wantMore(Limiter *l)
{
	waiting.insert(l);
}
/* }}} */

}; // namespace Net

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
