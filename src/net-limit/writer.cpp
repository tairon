/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include <tairon/net/socket.h>

#include "writer.h"

#include "limiter.h"

namespace Tairon
{

namespace Net
{

/* {{{ Writer::Writer(Socket *, Limiter *) */
Writer::Writer(Socket *s, Limiter *l) : limiter(l), offset(0), socket(s)
{
	writeFunctor = Tairon::Core::methodFunctor(this, &Writer::write);
}
/* }}} */

/* {{{ Writer::~Writer() */
Writer::~Writer()
{
	limiter->removeFromQueue(writeFunctor);
	delete writeFunctor;
}
/* }}} */

/* {{{ Writer::write() */
void Writer::write()
{
	size_t count = limiter->getAllowedSize(bufSize - offset);

	if (!count) // no more bytes allowed to write
		limiter->wantMore(writeFunctor);
	else {
		try {
			size_t len = socket->write(buffer + offset, count, false);
			if (len) {
				offset += len;
				limiter->transferred(len);
				if (offset == bufSize)
					dataWrittenSignal.emit(this);
				else // we still have something to send
					limiter->wantMore(writeFunctor);
			}
		} catch (const Tairon::Net::SocketException &) {
			socket->close();
			errorSignal.emit(this, socket);
		}
	}
}
/* }}} */

}; // namespace Net

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
