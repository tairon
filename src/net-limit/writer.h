/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_limit_writer_h
#define _tairon_net_limit_writer_h

#include <tairon/core/signals.h>

namespace Tairon
{

namespace Net
{

class Limiter;
class Socket;

/** \brief This class provides base class for writers.
 *
 * Writer is an object that writes data to a socket and limits outgoing
 * bandwidth according to its limiter.
 *
 * This class must not be used directly because the internal buffer must be set
 * correctly.
 */
class Writer
{
	public:
		/** Creates a Writer object.
		 *
		 * \param s Socket to which the Writer is writing.
		 * \param l Limiter used for limiting bandwidth.
		 */
		Writer(Socket *s, Limiter *l);

		/** Destroys the object.
		 */
		virtual ~Writer();

		/** Returns pointer to the socket used by this writer.
		 */
		Socket *getSocket() {
			return socket;
		};

		/** Writes data to the socket.
		 */
		void write();

	public:
		/** Emitted when all data from the interal buffer has been written to
		 * the socket.
		 */
		Tairon::Core::Signal1<void, Writer *> dataWrittenSignal;

		/** This signal is emitted when a socket error occurs. The socket is
		 * closed when the signal is emitted.
		 */
		Tairon::Core::Signal2<void, Writer *, Socket *> errorSignal;

	protected:
		/** Buffer with data to send.
		 */
		const char *buffer;

		/** Size of the buffer.
		 */
		size_t bufSize;

		/** Limiter used for limiting bandwidth.
		 */
		Limiter *limiter;

		/** Offset from the beginning of the buffer.
		 */
		size_t offset;

		/** Functor for calling write method.
		 */
		Tairon::Core::Functor0<void> *writeFunctor;

		/** Socket to which the Writer is writing.
		 */
		Socket *socket;
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
