/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_limit_reader_h
#define _tairon_net_limit_reader_h

#include <tairon/core/signals.h>

namespace Tairon
{

namespace Net
{

class Limiter;
class Socket;

/** \brief This class provides base class for readers.
 *
 * Reader is an object that reads data from a socket and limits incoming
 * bandwidth according to its Limiter.
 *
 * This class must not be used directly because the internal buffer must be set
 * correctly.
 */
class Reader
{
	public:
		/** Creates a Reader object.
		 *
		 * \param s Socket from which the Reader is reading.
		 * \param l Limiter used for limiting bandwidth.
		 */
		Reader(Socket *s, Limiter *l);

		/** Destroys the object.
		 */
		virtual ~Reader();

		/** Returns pointer to the internal buffer.
		 */
		const char *getBuffer() {
			return buffer;
		};

		/** Reads data from the socket.
		 */
		void read();

		/** Sets offset to the internal buffer to zero. It is useful for
		 * reusing this reader.
		 */
		void reset();

	public:
		/** Emitted when the internal buffer is full.
		 */
		Tairon::Core::Signal1<void, Reader *> bufferFullSignal;

		/** Emitted when there are new data in the buffer.
		 *
		 * The receiver gets pointer to this Reader as the first parameter and
		 * number of bytes read as the second one.
		 */
		Tairon::Core::Signal2<void, Reader *, size_t> dataReadSignal;

		/** This signal is emitted when a socket error occurs. It could also
		 * mean that the connection has been closed. The socket is already
		 * closed when the signal is emitted.
		 */
		Tairon::Core::Signal2<void, Reader *, Socket *> errorSignal;

	protected:
		/** Buffer for storing incoming data.
		 */
		char *buffer;

		/** Size of the buffer;
		 */
		size_t bufSize;

		/** Limiter used for limiting bandwidth.
		 */
		Limiter *limiter;

		/** Offset from the beginning of the buffer.
		 */
		size_t offset;

		/** Functor for calling read method.
		 */
		Tairon::Core::Functor0<void> *readFunctor;

		/** Socket from which the Reader is reading.
		 */
		Socket *socket;
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
