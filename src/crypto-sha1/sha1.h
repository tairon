/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_crypto_sha1_sha1_h
#define _tairon_crypto_sha1_sha1_h

#include <tairon/core/exceptions.h>
#include <tairon/core/string.h>

using Tairon::Core::String;

struct SHA1Context;

namespace Tairon
{

namespace Crypto
{

/** \brief Class for convenient computing SHA1 hash.
 */
class SHA1 {
	public:
		/** Initializes new object and resets it to the default values.
		 */
		SHA1();

		/** Frees all allocated resources.
		 */
		~SHA1();

		/** Convenient method that computes SHA1 hash of the given data.
		 */
		static String hash(const String &data) {
			SHA1 s;
			s.update(data);
			return s.final();
		};

		/** Resets computing context.
		 */
		void reset();

		/** Updates context according to the given string.
		 */
		void update(const String &data);

		/** Updates context according to the given data.
		 */
		void update(const char *data, size_t length);

		/** Computes hash.
		 */
		String final();

	private:
		/** Hash context.
		 */
		SHA1Context *ctx;
};


/** \brief Exception for SHA1 errors.
 */
class SHA1Exception : public Tairon::Core::Exception
{
	public:
		/** Standard constructor.
		 */
		SHA1Exception(const String &desc) : Tairon::Core::Exception(desc) {};

		/** Default destructor.
		 */
		virtual ~SHA1Exception() {};
};

}; // namespace Crypto

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
