#include "sha1.h"

#include "rfc-sha1.h"

namespace Tairon
{

namespace Crypto
{

/* {{{ SHA1::SHA1() */
SHA1::SHA1()
{
	ctx = new SHA1Context;
	SHA1Reset(ctx);
}
/* }}} */

/* {{{ SHA1::~SHA1() */
SHA1::~SHA1()
{
	delete ctx;
}
/* }}} */

/* {{{ SHA1::final() */
String SHA1::final()
{
	uint8_t digest[SHA1HashSize];
	SHA1Result(ctx, digest);
	return std::string((char *) digest, SHA1HashSize);
}
/* }}} */

/* {{{ SHA1::reset() */
void SHA1::reset()
{
	SHA1Reset(ctx);
}
/* }}} */

/* {{{ SHA1::update(const std::string &data) */
void SHA1::update(const String &data)
{
	update(data.data(), data.length());
}
/* }}} */

/* {{{ SHA1::update(const char *, size_t) */
void SHA1::update(const char *data, size_t length)
{
	if (SHA1Input(ctx, (const uint8_t *) data, length))
		throw SHA1Exception("Cannot update already finalized digest");
}
/* }}} */

}; // namespace Crypto

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
