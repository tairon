/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include <cctype>
#include <sstream>

#include <tairon/core/log.h>
#include <tairon/core/thread.h>
#include <tairon/net/socket.h>

#include "httpclient.h"

/* {{{ static const char hexTable[] */
static const char hexTable[] = {
	'0', '1', '2', '3', '4', '5', '6', '7',
	'8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
};
/* }}} */

namespace Tairon
{

namespace Net
{

/* {{{ HTTPClient::HTTPClient(const String &, uint16_t */
HTTPClient::HTTPClient(const String &h, uint16_t p) : host(h), port(p), socket(0)
{
}
/* }}} */

/* {{{ HTTPClient::~HTTPClient() */
HTTPClient::~HTTPClient()
{
}
/* }}} */

/* {{{ HTTPClient::clearParameters() */
void HTTPClient::clearParameters()
{
	parameters.clear();
}
/* }}} */

/* {{{ HTTPClient::close() */
void HTTPClient::close()
{
	if (!socket) // there is nothing to do
		return;

	socket->close();
	socket = 0;
}
/* }}} */

/* {{{ HTTPClient::connected(Tairon::Net::Socket *) */
void HTTPClient::connected(Tairon::Net::Socket *)
{
	DEBUG("HTTPClient::connected");
	uri += prepareParameters();

	try {
		socket->ready();
		socket->write("GET " + uri + " HTTP/1.0\r\nConnection: close\r\nHost: " + host + "\r\n\r\n");
	} catch (const SocketException &e) {
		close();
		errorSignal.emit(this, "Socket error", e.getErrorNumber());
		return;
	}

	dataReader = &HTTPClient::readStatus;
}
/* }}} */

/* {{{ HTTPClient::delParameter(const String &) */
void HTTPClient::delParameter(const String &key)
{
	parameters.erase(key);
}
/* }}} */

/* {{{ HTTPClient::error(Tairon::Core::Socket *, int) */
void HTTPClient::error(Tairon::Net::Socket *, int err)
{
	close();
	errorSignal.emit(this, "Error while connecting", err);
}
/* }}} */

/* {{{ HTTPClient::escape(const String &) */
String HTTPClient::escape(const String &what)
{
	unsigned int len = what.length();
	String out;
	out.reserve(len * 3);

	for (unsigned int i = 0; i < len; ++i)
		if (isalnum(what[i]) || (what[i] == '-'))
			out += what[i];
		else
			out += String("%", 1) + hexTable[(what[i] >> 4) & 0x0f] + hexTable[what[i] & 0x0F];

	return out;
}
/* }}} */

/* {{{ HTTPClient::get(const String &) */
void HTTPClient::get(const String &what)
{
	if (socket) {
		WARNING("Trying to get data when there is an active socket");
		return;
	}

	uri = what;

	// clear data, it may contain something from previous request
	data.clear();

	Tairon::Core::Thread *current = Tairon::Core::Thread::current();

	socket = new Socket(Socket::IPv4, Socket::Stream);
	socket->connectedSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &HTTPClient::connected));
	socket->errorSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &HTTPClient::error));
	socket->readyReadSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &HTTPClient::readyRead));

	try {
		socket->connect(host, port);
	} catch (const SocketException &e) {
		close();
		errorSignal.emit(this, (const char *) String(e), e.getErrorNumber());
	}
}
/* }}} */

/* {{{ HTTPClient::isActive() */
bool HTTPClient::isActive()
{
	return socket != 0;
}
/* }}} */

/* {{{ HTTPClient::prepareParameters() */
String HTTPClient::prepareParameters()
{
	String ret("?");
	for (std::map<String, String>::const_iterator it = parameters.begin(); it != parameters.end(); ++it)
		ret += it->first + "=" + it->second + "&";

	ret.erase(ret.length() - 1); // remove trailing &

	return ret;
}
/* }}} */

/* {{{ HTTPClient::readData() */
void HTTPClient::readData()
{
	// just empty function, body of a message from the server is incoming and
	// there is no need to parse it, the data is already stored
}
/* }}} */

/* {{{ HTTPClient::readHeader() */
void HTTPClient::readHeader()
{
	String line;
	while (data.canReadLine()) {
		line = data.readLine();
		line.rstrip();
		if (line.empty()) {
			dataReader = &HTTPClient::readData;
			break;
		}
	}
}
/* }}} */

/* {{{ HTTPClient::readStatus() */
void HTTPClient::readStatus()
{
	String line = data.readLine();
	if (line.empty())
		return;

	line.rstrip();

	std::stringstream s(line);
	String protocol;
	int code;

	s >> protocol;
	s >> code;

	if (protocol.find("HTTP") != 0) // unsupported protocol
		throw "Unsupported protocol";

	if (code != 200) // invalid return code
		throw "Invalid return code";
	// TODO: handle redirect codes

	dataReader = &HTTPClient::readHeader;
	readHeader(); // there may be another data, process them
}
/* }}} */

/* {{{ HTTPClient::readyRead(Tairon::Net::Socket *) */
void HTTPClient::readyRead(Tairon::Net::Socket *)
{
	try {
		data += socket->read();
	} catch (const Tairon::Net::SocketException &) {
		// connection has been closed or some other error occured
		socket->close();
		socket = 0;
	}

	try {
		(this->*dataReader)();
		if (!socket) // connection has been closed
			dataReadSignal.emit(this);
	} catch (const char *e) { // invalid data received
		close();
		errorSignal.emit(this, e, 0);
	}
}
/* }}} */

/* {{{ HTTPClient::setParameter(const String &, const String &) */
void HTTPClient::setParameter(const String &key, const String &value)
{
	parameters[key] = escape(value);
}
/* }}} */

}; // namespace Net

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
