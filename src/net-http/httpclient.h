/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_http_httpclient_h
#define _tairon_net_http_httpclient_h

#include <map>

#include <tairon/core/signals.h>
#include <tairon/core/string.h>

using Tairon::Core::String;

namespace Tairon
{

namespace Net
{

class Socket;

/** \brief Simple HTTP/1.0 client.
 *
 * This class provides simple client for fetching documents via GET method of
 * HTTP/1.0 protocol.
 */
class HTTPClient
{
	public:
		/** Creates new client object.
		 *
		 * \param h Server's hostname.
		 * \param p Server's port.
		 */
		HTTPClient(const String &h, uint16_t p = 80);

		/** Destroys the client.
		 */
		~HTTPClient();

		/** Clears parameters of the GET method.
		 */
		void clearParameters();

		/** Closes the connection to the server if it is opened.
		 */
		void close();

		/** Deletes given parameter from list.
		 */
		void delParameter(const String &key);

		/** Escapes characters in string so that it can be used in GET request.
		 */
		static String escape(const String &what);

		/** Requests a data via GET method. The data is processed in the
		 * current thread, so Tairon::Core::Thread::current() must return
		 * non-zero value and Tairon::Core::Thread::callFunctors() must be
		 * called in order to deliver signals to this class.
		 *
		 * \param what URI of the data.
		 */
		void get(const String &what);

		/** Returns data that has been read from the server.
		 */
		const String &getData() {
			return data;
		};

		/** Returns true if there is an active connection to the server;
		 * otherwise returns false.
		 */
		bool isActive();

		/** Sets parameter of the GET method.
		 */
		void setParameter(const String &key, const String &value);

	public:
		/** This signal is emitted when data from the server has been read and
		 * the connection has been closed.
		 */
		Tairon::Core::Signal1<void, HTTPClient *> dataReadSignal;

		/** This signal is emitted when an error occurs. The first parameter is
		 * description of the error and the second one is its error code.
		 */
		Tairon::Core::Signal3<void, HTTPClient *, const char *, int> errorSignal;

	private:
		/** Slot that is called when the connection to the server has been
		 * established.
		 */
		void connected(Tairon::Net::Socket *);

		/** Slot that is called when an socket error occured.
		 */
		void error(Tairon::Net::Socket *, int err);

		/** Takes all parameters from table and returns them as one string that
		 * can be used in GET request.
		 */
		String prepareParameters();

		/** Reads data of the request.
		 */
		void readData();

		/** Reads HTTP header.
		 */
		void readHeader();

		/** Reads returned status.
		 */
		void readStatus();

		/** Slot that is called when there are available data from the socket.
		 */
		void readyRead(Tairon::Net::Socket *);

	private:
		/** Data read from the server.
		 */
		String data;

		/** Method that actually parses data.
		 */
		void (HTTPClient::*dataReader)();

		/** Server's hostname.
		 */
		String host;

		/** Parameters that are passed to GET method.
		 */
		std::map<String, String> parameters;

		/** Server's port.
		 */
		uint16_t port;

		/** Connection to the server.
		 */
		Socket *socket;

		/** URI of the data to request.
		 */
		String uri;
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
