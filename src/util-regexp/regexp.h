/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_util_regexp_regexp_h
#define _tairon_util_regexp_regexp_h

#include <regex.h>

#include <tairon/core/exceptions.h>

using Tairon::Core::String;

namespace Tairon
{

namespace Util
{

/** \brief This class provides pattern matching using POSIX regular
 * expressions.
 *
 * Regular expressions provide a way to to find patterns withing text. It can
 * be useful in situations like validating, searching, searching and replacing
 * and string splitting.
 */
class RegExp
{
	public:
		/** Constructs a regular expression object.
		 *
		 * \param pattern Pattern that will be used for matching. See regex(7)
		 * for details.
		 */
		RegExp(const char *pattern);

		/** Destroys the object and frees all allocated resources.
		 */
		~RegExp();

		/** Attempts to find a match within a string. Returns position of the
		 * first match, or -1 if there is no match.
		 */
		int match(const String &s);

		/** Returns the text matched by the nth subexpression. The entire match
		 * has index 0 and the parenthesized subexpressions have indices
		 * starting from 1 (excluding non-capturing parentheses).
		 */
		String matched(int nth = 0) const;

		/** Returns length of the nth matched subexpression, or -1 if there is
		 * no such matched subexpression.
		 */
		unsigned int matchedLength(int nth) const;

		/** Returns starting position of the nth matched subexpression.
		 */
		unsigned int matchedStart(int nth) const;

	private:
		/** Size of the match buffer.
		 */
		int count;

		/** Buffer for matches.
		 */
		regmatch_t *pmatch;

		/** Pattern buffer storage area.
		 */
		regex_t preg;

		/** String that is searched for a match.
		 */
		String text;
};


/** \brief Exception that is used for RegExp errors.
 */
class RegExpException : public Tairon::Core::Exception
{
	public:
		/** Standard constructor.
		 */
		RegExpException(const String &desc) : Tairon::Core::Exception(desc) {};

		/** Default destructor.
		 */
		~RegExpException() {};
};

}; // namespace Util

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
