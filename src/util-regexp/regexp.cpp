/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include "regexp.h"

namespace Tairon
{

namespace Util
{

/* {{{ RegExp::RegExp(const char *) */
RegExp::RegExp(const char *pattern) : count(1)
{
	// compile regexp
	if (int err = regcomp(&preg, pattern, REG_EXTENDED)) {
		size_t len = regerror(err, &preg, 0, 0);
		char buf[len];
		regerror(err, &preg, buf, len);
		regfree(&preg);
		throw RegExpException("Cannot compile regular expression: " + String(buf, len));
	}

	// prepare matching buffer
	const char *p = pattern;
	while (*p) {
		if (*p == '(')
			++count;
		++p;
	}
	pmatch = new regmatch_t[count];
}
/* }}} */

/* {{{ RegExp::~RegExp() */
RegExp::~RegExp()
{
	delete [] pmatch;
	regfree(&preg);
}
/* }}} */

/* {{{ RegExp::match(const String &) */
int RegExp::match(const String &s)
{
	text = s;
	if (regexec(&preg, s, count, pmatch, 0))
		return -1;
	return pmatch[0].rm_so;
}
/* }}} */

/* {{{ RegExp::matched(int) const */
String RegExp::matched(int nth) const
{
	if ((nth < 0) || (nth > count))
		throw RegExpException("Index is out of bound");
	if (pmatch[nth].rm_so < 0)
		return String();
	return text.substr(pmatch[nth].rm_so, pmatch[nth].rm_eo - pmatch[nth].rm_so);
}
/* }}} */

/* {{{ RegExp::matchedLength(int nth) const */
unsigned int RegExp::matchedLength(int nth) const
{
	if ((nth < 0) || (nth > count))
		throw RegExpException("Index is out of bound");
	return pmatch[nth].rm_eo - pmatch[nth].rm_so;
}
/* }}} */

/* {{{ RegExp::matchedStart(int nth) const */
unsigned int RegExp::matchedStart(int nth) const
{
	if ((nth < 0) || (nth > count))
		throw RegExpException("Index is out of bound");
	return pmatch[nth].rm_so;
}
/* }}} */

}; // namespace Util

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
