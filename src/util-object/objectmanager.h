/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_util_objects_objectmanager_h
#define _tairon_util_objects_objectmanager_h

#include <map>

#include <tairon/core/mutex.h>
#include <tairon/core/string.h>

using Tairon::Core::String;

namespace Tairon
{

namespace Util
{

/** Holds information about one object.
 */
struct Object
{
	/** Number of references to this object.
	 */
	int refcount;

	/** Pointer to the object.
	 */
	void *object;
};

/** \brief Holds informations about allocated objects.
 *
 * This class shouldn't be used directly. Use getObject<TRes>(const String &)
 * and removeObject<TRes>(const String &) instead.
 */
class ObjectManager
{
	public:
		/** Construct new object manager.
		 */
		ObjectManager();

		/** Destroy the manager.
		 */
		~ObjectManager();

		/** Returns reference to a object with given name. If the object
		 * doesn't exist then new is created and its pointer and refcount is
		 * set to 0.
		 *
		 * \param name Name of the object.
		 */
		Object &getObject(const String &name);

		/** Locks internal mutex.
		 */
		void lock() {
			mutex->lock();
		};

		/** Removes object with given name.
		 */
		void removeObject(const String &name);

		/** Returns pointer to the instance of this class.
		 */
		static ObjectManager *self() {
			return objectManager;
		};

		/** Unlocks internal mutex.
		 */
		void unlock() {
			mutex->unlock();
		};

	private:
		/** Mutex for protecting access to this class.
		 */
		Tairon::Core::Mutex *mutex;

		/** Pointer to the instance of this class.
		 */
		static ObjectManager *objectManager;

		/** Mapping name->object.
		 */
		std::map<String, Object> objects;
};

/** Returns a object of type TObj with given name. If the object doesn't
 * exist then one is created with the default parameters (e.g. no parameter is
 * supplied to the constructor.
 *
 * \param name name Name of the object.
 *
 * \sa removeObject<TObj>(const String &)
 */
template<class TObj> TObj *getObject(const String &name)
{
	ObjectManager *rmgr = ObjectManager::self();
	rmgr->lock();

	Object &res = rmgr->getObject(name);
	if (res.refcount) {
		++res.refcount;
		rmgr->unlock();
		return (TObj *) res.object;
	} else {
		res.refcount = 1;
		res.object = new TObj();
		rmgr->unlock();
		return (TObj *) res.object;
	}
}

/** Frees object of type TObj with given name. If the object is no longer
 * needed then it is deleted.
 *
 * \param name Name of the object.
 *
 * \sa getObject<TObj>(const String &)
 */
template<class TObj> void removeObject(const String &name)
{
	ObjectManager *rmgr = ObjectManager::self();
	rmgr->lock();

	Object &res = rmgr->getObject(name);
	if (--res.refcount == 0) {
		delete (TObj *) res.object;
		rmgr->removeObject(name);
	}

	rmgr->unlock();
}
}; // namespace Util

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
