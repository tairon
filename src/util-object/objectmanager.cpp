/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include <tairon/core/mutex.h>

#include "objectmanager.h"

namespace Tairon
{

namespace Util
{

ObjectManager *ObjectManager::objectManager = 0;

/* {{{ ObjectManager::ObjectManager() */
ObjectManager::ObjectManager()
{
	mutex = new Tairon::Core::Mutex();
	objectManager = this;
}
/* }}} */

/* {{{ ObjectManager::~ObjectManager() */
ObjectManager::~ObjectManager()
{
	objectManager = 0;
	delete mutex;
}
/* }}} */

/* {{{ ObjectManager::getObject(const String &) */
Object &ObjectManager::getObject(const String &name)
{
	Object *ret;
	if (!objects.count(name)) {
		ret = &objects[name];
		ret->object = 0;
		ret->refcount = 0;
	} else
		ret = &objects[name];
	return *ret;
}
/* }}} */

/* {{{ ObjectManager::removeObject(const String &) */
void ObjectManager::removeObject(const String &name)
{
	objects.erase(name);
}
/* }}} */

}; // namespace Util

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
