/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <unistd.h>

#include <tairon/core/log.h>
#include <tairon/net/poll.h>
#include <tairon/net/socketnotifier.h>

#include "server.h"

namespace Tairon
{

namespace Net
{

/* {{{ Server::Server(const char *, uint16_t, unsigned int) */
Server::Server(const char *address, uint16_t port, unsigned int qLen)
{
	fd = socket(PF_INET, SOCK_STREAM, 0);
	if (fd == -1)
		throw ServerException("Cannot create socket", errno);

	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	if (address) {
		struct hostent *host = gethostbyname(address);
		if (!host)
			throw ServerException("Cannot resolve bind address", h_errno);

		if (host->h_addrtype != AF_INET)
			throw ServerException("Invalid address type", host->h_addrtype);

		if (!(*host->h_addr_list))
			throw ServerException("Zero length list of addresses", 0);

		memcpy(&(addr.sin_addr.s_addr), *(host->h_addr_list), sizeof(addr.sin_addr.s_addr));
	} else
		addr.sin_addr.s_addr = INADDR_ANY;

	if (bind(fd, (sockaddr *) &addr, sizeof(addr)) == -1) {
		int err = errno;
		close(fd);
		throw ServerException("Cannot bind the server socket", err);
	}

	if (listen(fd, qLen) == -1) {
		int err = errno;
		close(fd);
		throw ServerException("Cannot listen to the server socket", err);
	}

	if (fcntl(fd, F_SETFL, fcntl(fd, F_GETFL, 0) | O_NONBLOCK) == -1) {
		int err = errno;
		close(fd);
		throw ServerException("Cannot set non-blocking server socket", err);
	}

	notifier = new SocketNotifier(Tairon::Core::methodFunctor(this, &Server::newConnection), 0, 0);
}
/* }}} */

/* {{{ Server::Server(const String &, unsigned int) */
Server::Server(const String &path, unsigned int qLen)
{
	if (path.length() >= 108) // length of struct sockaddr_un.sun_path field
		throw ServerException("Path for unix socket is too long", 0);

	fd = socket(PF_UNIX, SOCK_STREAM, 0);
	if (fd == -1)
		throw ServerException("Cannot create socket", errno);

	// make sure the path we want to bind this socket doesn't already exist
	unlink(path);

	sockaddr_un addr;
	addr.sun_family = AF_UNIX;
	memcpy(addr.sun_path, path.data(), path.length());
	addr.sun_path[path.length()] = 0;

	if (bind(fd, (sockaddr *) &addr, sizeof(addr)) == -1) {
		int err = errno;
		close(fd);
		throw ServerException("Cannot bind the server socket", err);
	}

	if (listen(fd, qLen) == -1) {
		int err = errno;
		close(fd);
		throw ServerException("Cannot listen to the server socket", err);
	}

	if (fcntl(fd, F_SETFL, fcntl(fd, F_GETFL, 0) | O_NONBLOCK) == -1) {
		int err = errno;
		close(fd);
		throw ServerException("Cannot set non-blocking server socket", err);
	}

	notifier = new SocketNotifier(Tairon::Core::methodFunctor(this, &Server::newConnection), 0, 0);
}
/* }}} */

/* {{{ Server::~Server() */
Server::~Server()
{
	close(fd);
	newConnectionSignal.disable();
	delete notifier;
}
/* }}} */

/* {{{ Server::newConnection() */
void Server::newConnection()
{
	DEBUG("Server::newConnection");

	int sd = accept(fd, 0, 0);
	if (sd == -1)
		return;

	newConnectionSignal.emit(this, sd);
}
/* }}} */

/* {{{ Server::ready() */
void Server::ready()
{
	Tairon::Net::Poll::self()->registerSocketToRead(fd, notifier);
}
/* }}} */


/* {{{ ServerException::ServerException(const String &, int) */
ServerException::ServerException(const String &desc, int err) : Tairon::Core::Exception(desc), errorNumber(err)
{
	DEBUG((const char *) String(desc + " (" + String::number(errorNumber) + ")"));
}
/* }}} */

}; // namespace Net

}; // namespace Tairon

// vim: ai sw=4 ts=4 noet fdm=marker
