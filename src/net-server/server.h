/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation and appearing    *
 *   in the file LICENSE.LGPL included in the packaging of this file.      *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   Library General Public License for more details.                      *
 *                                                                         *
 ***************************************************************************/

#ifndef _tairon_net_server_server_h
#define _tairon_net_server_server_h

#include <tairon/core/exceptions.h>
#include <tairon/core/signals.h>

using Tairon::Core::String;

namespace Tairon
{

namespace Net
{

class SocketNotifier;

/** \brief Class that provides listening socket.
 *
 * This class is a convenience class for accepting incoming connections. The
 * connections can be through TCP or unix socket.  Length of queue for incoming
 * connections, listen address and bind port are passed to the constructor and
 * a signal is emitted when when a new connection is accepted.
 */
class Server
{
	public:
		/** Creates a server object for TCP server.
		 *
		 * \param address Address on which the server will listen. Use 0 for
		 * listening on all computer's addresses.
		 * \param port Port on which the server will listen.
		 * \param qLen Length of queue for connections that are waiting to be
		 * accepted.
		 */
		Server(const char *address, uint16_t port, unsigned int qLen);

		/** Creates a server object for unix socket.
		 *
		 * \param path Path to the unix socket.
		 * \param qLen Length of queue for connections that are waiting to be
		 * accepted.
		 */
		Server(const String &path, unsigned int qLen);

		/** Destroys the server. All pending connections that are still in
		 * queue are severed. Existing connections continue to exist.
		 */
		~Server();

		/** Tells this server that all setup has been done.
		 */
		void ready();

	public:
		/** Emitted when a new connection is accepted.
		 */
		Tairon::Core::DSignal2<Server *, int> newConnectionSignal;

	private:
		/** Called when a new connection is waiting for accept.
		 */
		void newConnection();

	private:
		/** Server's socket descriptor.
		 */
		int fd;

		/** Notifier for incoming connections.
		 */
		SocketNotifier *notifier;
};

/** \brief Exception for server errors.
 */
class ServerException : public Tairon::Core::Exception
{
	public:
		/** Constructor that takes as parameters description of the exception
		 * and its error number.
		 */
		ServerException(const String &desc, int err);

		/** Standard destructor.
		 */
		virtual ~ServerException() {};

		/** Returns error number of the exception.
		 */
		int getErrorNumber() const {
			return errorNumber;
		};

	private:
		/** Error number of this exception.
		 */
		int errorNumber;
};

}; // namespace Net

}; // namespace Tairon

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
