#!/bin/sh

find -regex '.*~' -exec rm -f \{\} \;
rm -rf doc/html
rm -f lib/*
jam clean
