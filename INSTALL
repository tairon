Tairon installation instructions
================================

Requirements
------------

1. Linux kernel version 2.6 (Tairon uses epoll interface)
2. Jam build tool - unlike many other programs Tairon uses jam for building.
   Jam can be downloaded at http://www.perforce.com/jam/jam.html
3. Sane build environment: C++ compiler (Tairon is tested with GCC 4.1)


Configuration
-------------

Because Tairon uses Jam, all configuration options are located in the Jamrules
file in the package's root directory. Edit this file in your favourite text
editor to adjust settings. There are some interesting variables:

PREFIX::
	This points to the base directory for installation. It defaults to
	/usr/local.

INCDIR::
	Directory where header files will be installed. Its default is
	$(PREFIX)/include directory.

LIBDIR::
	Directory where libraries will be installed. It defaults to
	$(PREFIX)/lib/tairon.

C++FLAGS::
	Flags that are passed to the compiler during building object (*.o)
	files.

DEBUG::
	If it is set to 1 then the library will be build with debug
	information and it will not use optimization (-O2 flag to the
	compiler).


Building and installing
-----------------------

To build the libraries run `jam` command in the package's root directory. To
install them run `jam install`. Note that this step may need root privileges if
you want to install the libraries system-wide. The final step may be editing
the /etc/ld.so.conf file and adding directory where the libraries are installed
(otherwise programs that want to use this library may not be able to find the
libraries at runtime) and running ldconfig program to update ls.so.cache file.
